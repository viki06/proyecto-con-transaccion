﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{

    
    public class MovimientoBRL : AbstractBRL
    {
        movimiento mov;
        movimientoDAL dal;
        public MovimientoBRL()
        {
            dal = new movimientoDAL();
        }
        public MovimientoBRL(movimiento mov)
        {
            this.mov = mov;
            dal = new movimientoDAL(this.mov);
        }
        public movimiento Mov { get => mov; set => mov = value; }
        public movimientoDAL Dal { get => dal; set => dal = value; }

        public override void Delete()
        {
            Dal.Delete();
        }

        public override void Insert()
        {
            Dal.Insert();

        }

        public override DataTable Select()
        {
         return   Dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }
    }
}
