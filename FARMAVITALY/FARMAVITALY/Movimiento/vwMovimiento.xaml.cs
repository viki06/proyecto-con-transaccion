﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using DAL;


namespace FARMAVITALY.Movimiento
{
    /// <summary>
    /// Lógica de interacción para vwMovimiento.xaml
    /// </summary>
    public partial class vwMovimiento : Window
    {
        public vwMovimiento()
        {
            InitializeComponent();
        }
        ventaBRL brlVenta;
        cliente client;
        producto product;
        venta Vent;
        List<detalleVenta> detalle;
        conversion c = new conversion();
        void FillIdAutoencremtVenta()
        {
            try
            {
                brlVenta = new ventaBRL();
                DgvIdAutoIncremetVent.ItemsSource = null;
                DgvIdAutoIncremetVent.ItemsSource = brlVenta.SelectAutoIncrementIdVenta().DefaultView;
                DgvIdAutoIncremetVent.Columns[0].Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        void datosCliente()
        {
            client = null;
            if (Generica.idClient != 0)
            {
                client = new cliente(Generica.idClient, Generica.nit, Generica.razonSocial);
                txtIdCliente.Text = client.IdCliente.ToString();
                txtNit.Text = client.Nit;
                txtRazonSocial.Text = client.RazonSocial;
            }
        }
        void ClearCliente()
        {
            txtNit.Text = "";
            txtRazonSocial.Text = "";
        }

        void clearTextProducto()
        {
            txtNombreProducto.Text = "";
            txtPresentacion.Text = "";
            txtStock.Text = "";
            txtPrecioVenta.Text = "";
            txtCantidad.Text = "";
        }
        void datosProducto()
        {
            product = null;
            if (Generica.idProduct != 0)
            {
                product = new producto(Generica.idProduct, Generica.nombreProduct, Generica.stock, Generica.precioVent, Generica.presentation);

                txtIdProducto.Text = product.IdProducto.ToString();
                txtNombreProducto.Text = product.NombreProducto;
                txtStock.Text = product.Stock.ToString();
                txtPresentacion.Text = product.Presentacion;
                txtPrecioVenta.Text = product.PrecioVenta.ToString();
            }
        }
        private void TxtBuscarCliente_Click(object sender, RoutedEventArgs e)
        {
            Cliente.vwCliente vw = new Cliente.vwCliente();
            vw.ShowDialog();
        }
        void totalLetras()
        {
            txtTotalDescuento.Text = c.enletras(txtTotalImporte.Text).ToLower();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblNombreUsuario.Content = "ATENDIDO POR :" + Sesion.rolSesion;
            FillIdAutoencremtVenta();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            datosCliente();
            datosProducto();
        }

        private void TxtBuscarProducto_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwBuscarProducto vw = new Producto.vwBuscarProducto();
            vw.ShowDialog();
        }
        void DatosCliente()
        {
            txtNit.Text = "Desconocido";
            txtRazonSocial.Text = "Desconocido";
        }
        void CleartxtPagos()
        {
            txtTotalDescuento.Text = "";
            txtTotalImporte.Text = "";
        }
        public decimal calcularTotal()
        {
            try
            {
                decimal sum = 0;
                TextBlock x;
                for (int i = 0; i < DgvDatos.Items.Count; i++)
                {
                    x = DgvDatos.Columns[8].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                 
                    sum += decimal.Parse(x.Text);
                    txtTotalImporte.Text = sum.ToString();
                }
                return sum;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return -1;
            }
        }
        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductoVenta item = new ProductoVenta();
                item.idUsuario = Sesion.idSesion;
                item.idCliente = int.Parse(txtIdCliente.Text);
                item.idProducto = int.Parse(txtIdProducto.Text);
                item.Nombre = txtNombreProducto.Text;
                item.Presentacion = txtPresentacion.Text;
                item.PreVenta = decimal.Parse(txtPrecioVenta.Text);
                item.CantProducto = int.Parse(txtCantidad.Text);
                item.importe = item.PreVenta * item.CantProducto;

                item.Stock = int.Parse(txtStock.Text);
                item.stockActual = item.Stock - item.CantProducto;

                DgvDatos.Items.Add(item);
                DgvDatos.UpdateLayout();
                
                decimal aux = calcularTotal();
                totalLetras();
                
                clearTextProducto();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void RbtnVenta_Click(object sender, RoutedEventArgs e)
        {
            if (DgvDatos.Items.Count > 0)
            {
                try
                {
                    if (MessageBox.Show("Esta Segura@ De Realizar la venta..??", "Vender", MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Vent = new venta(decimal.Parse(txtTotalImporte.Text), 0, Sesion.idSesion, client.IdCliente);
                        detalle = new List<detalleVenta>();
                        TextBlock precioVenta, cantidad, idProducto;
                        for (int i = 0; i < DgvDatos.Items.Count; i++)
                        {
                            precioVenta = DgvDatos.Columns[6].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            cantidad = DgvDatos.Columns[7].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            idProducto = DgvDatos.Columns[3].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            detalleVenta det = new detalleVenta(decimal.Parse(precioVenta.Text), int.Parse(cantidad.Text), int.Parse
                                (idProducto.Text), Vent.IdVenta);
                            detalle.Add(det);
                        }
                    }

                    brlVenta = new ventaBRL(Vent, detalle);
                    brlVenta.Insert();

                    MessageBox.Show("La Venta Realizado Con Exito ");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }

            else
            {
                MessageBox.Show("Debe requerir un Medicamento");
            }
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DgvDatos.SelectedIndex >= 0)
                {
                    if (MessageBox.Show("Esta Segura@ De Eliminar Esta Fila..??", "ELIMINAR", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        for (int i = 0; i <= DgvDatos.SelectedItems.Count; i++)
                        {
                            DgvDatos.Items.Remove(DgvDatos.SelectedItems[i]);
                            decimal aux = calcularTotal();
                            totalLetras();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe Seleccionar una fila se desea Eliminar");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RbtnCaducidad_Click(object sender, RoutedEventArgs e)
        {
            DatosCliente();
        }

        private void RbtnMerma_Click(object sender, RoutedEventArgs e)
        {
            DatosCliente();
            if (DgvDatos.Items.Count > 0)
            {
                try
                {
                    if (MessageBox.Show("Esta Segura@ De Realizar Un Registro de Merma.??", "Registrado por Merma", MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Vent = new venta(decimal.Parse(txtTotalImporte.Text), 0, Sesion.idSesion, client.IdCliente);
                        detalle = new List<detalleVenta>();
                        TextBlock precioVenta, cantidad, idProducto;
                        for (int i = 0; i < DgvDatos.Items.Count; i++)
                        {
                            precioVenta = DgvDatos.Columns[6].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            cantidad = DgvDatos.Columns[7].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            idProducto = DgvDatos.Columns[3].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            detalleVenta det = new detalleVenta(decimal.Parse(precioVenta.Text), int.Parse(cantidad.Text), int.Parse
                                (idProducto.Text), Vent.IdVenta);
                            detalle.Add(det);
                        }
                    }
                    brlVenta = new ventaBRL(Vent, detalle);
                    brlVenta.Insert();

                    MessageBox.Show("La Venta Realizado Con Exito ");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }

            else
            {
                MessageBox.Show("Debe requerir un Medicamento");
            }
        }
        private void RbtnRobo_Click(object sender, RoutedEventArgs e)
        {
            DatosCliente();
        }

        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            FillIdAutoencremtVenta();
            clearTextProducto();
            ClearCliente();
            DgvDatos.Items.Clear();
            CleartxtPagos();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
          if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    ProductoVenta PV = new ProductoVenta();
                    PV.Nombre = txtNombreProducto.Text;
                    PV.Presentacion = txtPresentacion.Text;
                    PV.Stock = int.Parse(txtStock.Text);
                    PV.PreVenta = int.Parse(txtPrecioVenta.Text);
                    PV.CantProducto = int.Parse(txtCantidad.Text.ToString());
                    DgvDatos.SelectedItem = PV;

                    MessageBox.Show("Modificado  correctamente:");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("no funciona :" + ex.Message);
                }

            }
        }
    }

}
   