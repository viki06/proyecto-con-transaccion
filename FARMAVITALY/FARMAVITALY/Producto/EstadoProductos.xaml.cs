﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using System.Data;
using BRL;

namespace FARMAVITALY.Producto
{
    /// <summary>
    /// Lógica de interacción para EstadoProductos.xaml
    /// </summary>
    public partial class EstadoProductos : Window
    {
        Venta.vwVenta vwVenta;
        DataTable dt;
        Common.producto producto;
        ProductoBRL brl;
      

        void Cargardatos()
        {
            try
            {
                brl = new ProductoBRL();
                dgvDatos.ItemsSource = null;
                dgvDatos.ItemsSource = brl.SeleccionarProducto(producto.IdProducto).DefaultView;
                dgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        public EstadoProductos(Common.producto producto, DataTable dt)
        {
            InitializeComponent();
            this.producto = producto;
            this.dt = dt;

            Cargardatos();
        }

        public EstadoProductos( Common.producto producto, DataTable dt, Venta.vwVenta vwVenta)
        {
            InitializeComponent();
            this.producto = producto;
            this.dt=dt;
            this.vwVenta = vwVenta;


        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvDatos.Items.Count > 0 && txtCantidad.Text != "")
            {
                try
                {
                //    EjemplarProducto ej = new EjemplarProducto();
                //    ej.IdProducto = int.Parse(dgvDatos.CurrentRow.Cells[0].Value.ToString());
                //    ej.NombreProducto = dgvDatos.CurrentRow.Cells[2].Value.ToString();
                //    ej.Presentacion = dgvDatos.CurrentRow.Cells[3].Value.ToString();
                //    ej.PrecioVenta = dgvDatos.CurrentRow.Cells[5].Value.ToString();
                //    ej.TotalPago = (decimal.Parse(txtCantidad.Text) * float.Parse(dgvDatos.Cells[6].Value.ToString()));
                //    ej.Descuento = decimal.Parse(txtCantidad.Text) / decimal.Parse(dgvDatos.Cells[6].Value.ToString());

                //    MessageBox.Show("venta realizada con exito");
                }


                catch (Exception ex)
                {
                    MessageBox.Show("venta realizada con exito" + ex.Message);
                }
         
        }
    }
     
        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Cargardatos();
        }
    }
}
