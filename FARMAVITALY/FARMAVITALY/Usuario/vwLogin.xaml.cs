﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace FARMAVITALY.Login
{
    /// <summary>
    /// Lógica de interacción para vwLogin.xaml
    /// </summary>
    public partial class vwLogin : Window
    {
        public vwLogin()
        {
            InitializeComponent();
        }



        UsuarioBRL brl;
        byte contador = 0;


        private void BtnIngresar_Click(object sender, RoutedEventArgs e)
        {
            if (contador >= 3)
            {
                MessageBox.Show("Supero la cantidad de intentos permitidos el login");
                this.Close();
            }
            else
            {
                //    txtNombreUsuario.Text = txtNombreUsuario.Text.Trim();
                if (txtNombreUsuario.Text != "" && txtPassword.Password != "")
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        brl = new UsuarioBRL();
                        dt = brl.Login(txtNombreUsuario.Text, txtPassword.Password);
                        if (dt.Rows.Count > 0)
                        {
                            //Sesion.idSesion = int.Parse(dt.Rows[0][0].ToString());
                            //Sesion.usuarioSesion = dt.Rows[0][1].ToString();
                            //Sesion.rolSesion = dt.Rows[0][2].ToString();
                            //Sesion.passwordSesion = dt.Rows[0][3].ToString();

                            this.Visibility = Visibility.Hidden;
                            MainWindow win = new MainWindow();
                            win.Show();
                            this.Close();
                        }

                        else
                        {
                           
                            lblPasswordIncorrecto.Content = "Usuario y/o Contraseña Incorrecto : " + contador;
                            contador++;

                            txtNombreUsuario.Text = "";
                            txtPassword.Password = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Debe ingresar un nombre de Usuario y Contraseña");
                    contador++;
                }
            }
        }
        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
       }
 private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            MainWindow vw = new MainWindow();
            vw.ShowDialog();
        }

        private void Ing_Click(object sender, RoutedEventArgs e)
        {
            MainWindow vw = new MainWindow();
            vw.ShowDialog();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TxtRecuperarPassword_Click(object sender, RoutedEventArgs e)
        {
            Usuario.RecuperarPassword vw = new Usuario.RecuperarPassword();
            vw.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
     }
}
