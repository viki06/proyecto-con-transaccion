﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using Common;
using DAL;

namespace FarmaVitality.login
{
    /// <summary>
    /// Lógica de interacción para login.xaml
    /// </summary>
    public partial class login : Window
    {

       
        UsuariosBRL brl;
        byte contador = 0;
        public login()
        {
            InitializeComponent();
        }

        private void BtnIngresar_Click(object sender, RoutedEventArgs e)
        {
            if (contador > 3)
            {
                MessageBox.Show("supero la cantidad de intentos permitidos el login");
                this.Close();
            }
            else
            { 
            //txtNombreUsuario.Text = txtNombreUsuario.Text.Trim();
            if (txtNombreUsuario.Text != ""  && txtPassword.Password != "")
            {
                try
                {
                    DataTable dt = new DataTable();
                    brl = new UsuariosBRL();
                    dt = brl.Login(txtNombreUsuario.Text, txtPassword.Password);
                        if (dt.Rows.Count > 0)
                        {
                            Sesion.idSesion = int.Parse(dt.Rows[0][0].ToString());
                            Sesion.usuarioSesion = dt.Rows[0][1].ToString();
                            Sesion.rolSesion = dt.Rows[0][2].ToString();
                            //Sesion.passwordSesion = dt.Rows[0][3].ToString();

                            this.Visibility = Visibility.Hidden;
                            MainWindow win = new MainWindow();
                            win.Show();
                        }


                        else
                        {

                            lblInfo.Content = "  usuario y/o  contraseña incorrecto : " + contador;
                            contador++;
                            txtNombreUsuario.Text = "";
                            txtPassword.Password = "";

                        }

                    }
               
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                }
            }
          
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnIngresar_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Ing_Click(object sender, RoutedEventArgs e)
        {
            MainWindow vw = new MainWindow();
            vw.ShowDialog();
        }
    }
    }

