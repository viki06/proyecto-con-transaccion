﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;



namespace BRL
{
    public class ventaBRL : AbstractBRL
    {
        private  venta vent;
        private ventaDAL dal;
        private List<detalleVenta> detalle;
        public venta Vent { get => vent; set => vent = value; }
        public ventaDAL Dal { get => dal; set => dal = value; }
        public List<detalleVenta> Detalle { get => detalle; set => detalle = value; }

        public ventaBRL(venta vent,List<detalleVenta> detalle )
        {
            this.vent=vent;
            this.detalle = detalle;
            dal=new ventaDAL (this.vent,this.Detalle); // instanciar dal

        }
        public ventaBRL()
        {
            dal = new ventaDAL();
        }
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }   

        public DataTable SelectAutoIncrementIdVenta()
        {
            return dal.SelectAutoIncrementIdVenta();

        }
      
        public override void Update()
        {
            dal.Update();
        }
        public venta Get(int idVenta)
        {
            return dal.Get(idVenta);
        }
        public DataTable SelectVenta()
        {
            return dal.SelectVenta();
         }
       public int getIdVenta()
        {
            return dal.GetAutoincrementIdVenta();

        }

    }
    }
