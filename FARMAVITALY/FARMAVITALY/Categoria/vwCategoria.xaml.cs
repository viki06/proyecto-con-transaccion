﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace FARMAVITALY.Categoria
{
    /// <summary>
    /// Lógica de interacción para vwCategoria.xaml
    /// </summary>
    public partial class vwCategoria : Window
    {
        public vwCategoria()
        {
            InitializeComponent();
        }
        byte opcion = 0;
        Common.categoria cat;
        CategoriaBRL brl;


        void ClearTxt()
        {
            txtBuscarCategoria.Text = "";
            txtNombre.Text = "";
            txtDescripcion.Text = "";
        }

        void Habilitar(byte op)
        {
            this.opcion = op;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;
            txtNombre.Focus();



        }

        void DesHabilitar()
        {

            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;
        }
        void FillDataGrid()
        {
            try
            {
                brl = new CategoriaBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        void FillBuscar()
        {
            try
            {
                brl = new CategoriaBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.BuscarCategoria(txtBuscarCategoria.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (DgvDatos.SelectedItem != null && cat != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new CategoriaBRL(cat);
                        brl.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }


        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (opcion)
            {
                case 1:

                    try
                    {
                        cat = new Common.categoria(txtNombre.Text, txtDescripcion.Text);
                        brl = new CategoriaBRL(cat);
                        brl.Insert();
                        MessageBox.Show("categoria insertado con exito...");
                        FillDataGrid();
                        DesHabilitar();
                         ClearTxt();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                    break;

                case 2:
                    try
                    {
                        cat.Nombre = txtNombre.Text;
                        cat.Descripcion = txtDescripcion.Text;

                        brl = new CategoriaBRL(cat);
                        brl.Update();
                        MessageBox.Show(" categoria Modificado con exito...");
                        FillDataGrid();
                        DesHabilitar();
                        ClearTxt();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar " + ex.Message);
                    }
                    break;
            }
        }


        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
            FillBuscar();
        }

            private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            cat = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new CategoriaBRL();
                    cat = brl.Get(id);
                    if (cat != null)
                    {
                        txtNombre.Text = cat.Nombre;
                        txtDescripcion.Text = cat.Descripcion;

                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error en el get " + ex.Message);
                }

            }
        }

       

        private void txtBuscarCategoria_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscarCategoria.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscarCategoria.Text.Length >= 3)
                {
                    FillBuscar();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }
    }
    }




        
    

