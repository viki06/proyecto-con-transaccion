﻿using Common;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace DAL
{
    public class ProveedorDAL : AbstractDAL
    {
        private proveedor proveed;
        private producto product;
        private List<detalleproveedor> detalle; 

        public proveedor Proveed { get => proveed; set => proveed = value; }
        public List<detalleproveedor> Detalle { get => detalle; set => detalle = value; }
        public producto Product { get => product; set => product = value; }

        //estamos instanciando
        public ProveedorDAL(proveedor proveed,List<detalleproveedor> detalle)
        {
            this.proveed = proveed;
            this.detalle = detalle;

        }
        public ProveedorDAL(proveedor proveed)
        {
            this.proveed = proveed;

        }
        public ProveedorDAL()
        {

        }
        public DataTable SelectIDNameProveedor()
        {

            DataTable res = new DataTable();
            string query = @"SELECT idProveedor,nombre,primerApellido,segundoApellido
                            FROM proveedor
                            where estado = 1
                            order by 2";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return res;
        }
        public override void Delete()
        {
            string query = @"update proveedor set estado=0
                             where idProveedor=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", proveed.IdProveedor);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Insert()
        {
            string queryProveedor = @"insert into proveedor (nombre,primerApellido,segundoApellido,telefono,latitud,longitud,correoElectronico)
                                           values (@nombre,@primerApellido,@segundoApellido,@telefono,@latitud,@longitud,@correoElectronico)";

            string detalleProveedor = @"insert into detalleproveedor( idProducto, idProveedor)
                                        values( @idProducto, @idProveedor)";

            List<MySqlCommand> cmds = new List<MySqlCommand>();

            try
            {
                cmds = Methods.CreateNBasicCommand(1 + detalle.Count);
                cmds[0].CommandText = queryProveedor;
                cmds[0].Parameters.AddWithValue("@Nombre", proveed.Nombre);
                cmds[0].Parameters.AddWithValue("@primerApellido", proveed.PrimerApellido);
                cmds[0].Parameters.AddWithValue("@segundoApellido", proveed.SegundoApellido);
                cmds[0].Parameters.AddWithValue("@telefono", proveed.Telefono);
                cmds[0].Parameters.AddWithValue("@latitud", proveed.Latitud);
                cmds[0].Parameters.AddWithValue("@longitud", proveed.Longitud);
                cmds[0].Parameters.AddWithValue("@correoElectronico", proveed.CorreoElectronico);

                int id = Methods.GetAutoIncrementTable("producto");

                for (int i = 1; i < detalle.Count;)
                {
                    cmds[i].CommandText = detalleProveedor;
                    cmds[i].Parameters.AddWithValue("@idProveedor", detalle[i-1].IdProveedor);
                    cmds[i].Parameters.AddWithValue("@idProducto",id);
                }
                Methods.ExecuteNBasicComandTran(cmds); 
 
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable SelectBuscarProveedor(string texto)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idProveedor,nombre,primerApellido,segundoApellido,telefono,correoElectronico
                                 FROM proveedor
                                WHERE nombre LIKE @texto OR primerApellido LIKE @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%"+texto+"%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT idProveedor, nombre AS 'NOMBRE',primerApellido AS 'PRIMER APELLIDO' ,segundoApellido AS 'SEGUNDO APELLIDO',telefono AS 'TELEFONO',correoElectronico AS 'CORREO',CONCAT(latitud ,'', longitud ) AS 'DIRECCION DEL LABORATORIO' ,fechaActualizacion  AS  'FECHA DE ACTUALIZACION'
                            	FROM proveedor	 
                                WHERE estado=1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {
            string query = @"update SET nombre=@nombre,primerApellido=@primerApellido,segundoApellido=@segundoApellido,telefono=@telefono,latitud=@latitud,longitud=@longitud ,fechaActualizacion=CURRENT_TIMESTAMP
                                where idProveedor = @id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", proveed.IdProveedor);
                cmd.Parameters.AddWithValue("@nombre", proveed.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", proveed.PrimerApellido);
                cmd.Parameters.AddWithValue("@segundoApellido", proveed.SegundoApellido);
                cmd.Parameters.AddWithValue("@telefono", proveed.Telefono);
                cmd.Parameters.AddWithValue("@latitud", proveed.Latitud);
                cmd.Parameters.AddWithValue("@longitud", proveed.Longitud);
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public proveedor Get(int idProveedor)
        {
            proveedor res = null;

            string query = @" SELECT idProveedor,nombre,primerApellido,segundoApellido,telefono,latitud,longitud,correoElectronico,estado,fechaActualizacion
                                    FROM proveedor
                                    where idProveedor=@id";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idProveedor);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new proveedor(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(),int.Parse(dr[4].ToString()), double.Parse(dr[5].ToString()),double.Parse( dr[6].ToString()), dr[7].ToString(), byte.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()));
                }

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }

    }
}
