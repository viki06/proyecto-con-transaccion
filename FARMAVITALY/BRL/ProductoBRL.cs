﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class ProductoBRL : AbstractBRL
    {
        ProductoDAL dal;
        producto product;
        public int idProducto = 0;

        public ProductoDAL Dal { get => dal; set => dal = value; }
        public producto Product { get => product; set => product = value; }
       
        public ProductoBRL()
        {
            dal = new ProductoDAL();
        }
        public ProductoBRL(producto product)
        {
            this.Product = product;
            dal = new ProductoDAL(product);
        }
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }
        public DataTable ListaProducto()
        {
            return dal.ListaProducto();
        }
        public DataTable SelectStockProducto(int id)
        {
            return dal.SelectStockProducto(id);
        }
        
        public DataTable BuscarProductoNombre(string texto)
        {
            return dal.BuscarProductoNombre(texto);
        }
        public DataTable SeleccionarProducto(int id)
        {
            return dal.SeleccionarProducto(id);
        }
        
        public producto Get(int idProducto)
        {
            return dal.Get(idProducto);
        }
    }
}
