﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class CategoriaBRL : AbstractBRL
    {
        categoria cat;
        CategoriaDAL dal;

        public categoria Cat
        {
            get  {  return cat; }
            set  { cat = value;  }
        }

        public CategoriaDAL Dal
        {
            get     { return dal;}
             set  {  dal = value;   }
        }

      


        public CategoriaBRL()
        {
            dal = new CategoriaDAL();
        }
        public CategoriaBRL(categoria cat)
        {
            this.cat = cat;
             dal = new CategoriaDAL(this.cat);
        }



        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }
         public DataTable SelectIDName()
           {
            dal = new CategoriaDAL();
               return dal.SelectIDName();
           }
           

         public categoria Get(int idCategoria)
           {
               return dal.Get(idCategoria);
           }
        public DataTable BuscarCategoria(string texto)
        {
            return dal.BuscarCategoria(texto);
        }


    }
}