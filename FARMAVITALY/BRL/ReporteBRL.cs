﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BRL
{
    public class ReporteBRL : AbstractBRL
    {
        ClienteDAL dal;

        public ClienteDAL Dal { get => dal; set => dal = value; }

        public ReporteBRL()
        {
            dal = new ClienteDAL();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override void Insert()
        {
            throw new NotImplementedException();
        }

        public override DataTable Select()
        {
            return Dal.Select();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
