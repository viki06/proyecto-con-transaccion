﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class ClienteDAL : AbstractDAL
    {
        private   cliente client;

        public cliente Client { get => client; set => client = value; }

        public ClienteDAL(cliente client)
        {
            this.client = client;
        }
        public ClienteDAL()
        {
        }
        public override void Delete()
        {

            string query = @"update cliente set estado=0
                             where idCliente=@idCliente";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idCliente", Client.IdCliente);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Insert()
        {
            string query = @"insert into cliente (nit,razonSocial)
                                     values(@nit, @razonSocial)";

            MySqlCommand cmd;
            try
            {
                
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nit", client.Nit);
                cmd.Parameters.AddWithValue("@razonSocial", client.RazonSocial);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT idCliente,nit,razonSocial,FechaActualizacion FROM cliente
                    WHERE estado =1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public string SelectBuscarCliente(string texto)
        {

            string SelectClient;
            string query = @"select * from vwCliente
                        WHERE estado =1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" +texto+ "%");
                SelectClient = Methods.ExecuteScalarCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return SelectClient;

        }


        public DataTable BuscarClienteNit(string texto)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idCliente,nit,razonSocial
                                FROM cliente
                                WHERE nit  LIKE @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%"+texto+"%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        public override void Update()
        {
            string query = @"update cliente set   nit=@nit,     razonSocial=@razonSocial ,FechaActualizacion=CURRENT_TIMESTAMP
                                where idCliente = @id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", client.IdCliente);
                cmd.Parameters.AddWithValue("@nit", client.Nit);
                cmd.Parameters.AddWithValue("@razonSocial", client.RazonSocial);
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public cliente  Get(int idCliente)
        {
            
                cliente res = null;

                string query = @"SELECT idCliente,nit,razonSocial,estado ,FechaActualizacion  
                            FROM cliente
                            WHERE idCliente=@id";

                MySqlCommand cmd = null;
                MySqlDataReader dr = null;

                try
                {
                    cmd = Methods.CreateBasicCommand(query);
                    cmd.Parameters.AddWithValue("@id",idCliente);
                    dr = Methods.ExecuteDataReaderCommand(cmd);

                    while (dr.Read())
                    {
                        res = new cliente(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), byte.Parse(dr[3].ToString()), DateTime.Parse(dr[4].ToString()));
                    }

                }
                catch (Exception ex)
                {


                    throw ex;
                }
                finally
                {
                    dr.Close();
                    cmd.Connection.Close();
                }

                return res;
              }

          }
        }
    
