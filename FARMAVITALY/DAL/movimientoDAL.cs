﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using MySql.Data.MySqlClient;
using Common;

namespace DAL
{
    public class movimientoDAL : AbstractDAL
    {
        movimiento mov;
        //movimientoDAL dal;

        public movimiento Mov { get => mov; set => mov = value; }

        public movimientoDAL(movimiento mov)
        {
            this.mov = mov;
        }
        public movimientoDAL()
        {

        }
       

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override void Insert()
        {


            string queryMovimiento = @" insert into movimiento (tipoMovimiento,subTipo,cantidad,nroLote,fechaCaducidad,precioUnitario,idProducto)
                                                         values (@tipoMovimiento,@subTipo,@cantidad,@nroLote,@fechaCaducidad,@precioUnitario,@idProducto)";

            //string queryProducto = @"insert into producto(nombreProducto, precioCompra, precioVenta, stock, presentacion, foto,  idTipoProducto, idCategoria)
            //                                       values(@nombreProducto, @precioCompra, @precioVenta, @stock, @presentacion, @foto, @idTipoProducto, @idCategoria)";

            List<MySqlCommand> cmds = new List<MySqlCommand>();

            try
            {
                cmds = Methods.CreateNBasicCommand(1);
                //cmds[0].CommandText = queryProducto;
                //cmds[0].Parameters.AddWithValue("@nombreProducto", Product.NombreProducto);
                //cmds[0].Parameters.AddWithValue("@precioCompra", Product.PrecioCompra);
                //cmds[0].Parameters.AddWithValue("@precioVenta", Product.PrecioVenta);
                //cmds[0].Parameters.AddWithValue("@stock", Product.Stock);
                //cmds[0].Parameters.AddWithValue("@presentacion", Product.Presentacion);
                //cmds[0].Parameters.AddWithValue("@foto", Product.Foto);
                //cmds[0].Parameters.AddWithValue("@idTipoProducto", Product.IdTipoProducto);
                //cmds[0].Parameters.AddWithValue("@idCategoria", Product.IdCategoria);

                cmds[0].CommandText = queryMovimiento;
                cmds[0].Parameters.AddWithValue("@tipoMovimiento", Mov.TipoMovimiento);
                cmds[0].Parameters.AddWithValue("@subTipo", Mov.SubTipo);
                cmds[0].Parameters.AddWithValue("@cantidad", Mov.Cantidad);
                cmds[0].Parameters.AddWithValue("@nroLote", Mov.NroLote);
                cmds[0].Parameters.AddWithValue("@fechaCaducidad", Mov.FechaCaducidad);
                cmds[0].Parameters.AddWithValue("@precioUnitario", Mov.PrecioUnitario);
                cmds[0].Parameters.AddWithValue("@idProducto", Mov.IdProducto);

                int id = Methods.GetAutoIncrementTable("producto");
                cmds[1].Parameters.AddWithValue("@idProducto", id);

                Methods.ExecuteNBasicComandTran(cmds);
                  
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    

    public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT idmovimiento ,tipoMovimiento AS 'TIPO DE MOVIMIENTO',subTipo AS 'SUB TIPO',cantidad AS 'CANTIDAD',nroLote AS 'NUMERO DE LOTE',fechaCaducidad  AS  'FECHA DE CADUCIDAD',precioUnitario AS 'PRECIO POR UNIDAD',fechaActualizacion AS 'FECHA ACTUALIZACION'
                              FROM movimiento
                                WHERE estado=1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
