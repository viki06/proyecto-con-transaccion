﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class ClienteBRL : AbstractBRL
    {
        cliente client;
        ClienteDAL dal;

        public cliente Client { get => client; set => client = value; }
        public ClienteDAL Dal { get => dal; set => dal = value; }
        public ClienteBRL()
        {
            dal = new ClienteDAL();
        }
        public ClienteBRL(cliente Client)
        {
            this.Client = Client;
            dal = new ClienteDAL(this.Client);
        }

        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }
        public DataTable BuscarClienteNit(string texto)
        {
            return dal.BuscarClienteNit(texto);
        }
        public override void Update()
        {
            dal.Update();
        }
        public cliente Get(int idCliente)
        {
            return dal.Get(idCliente);
        }
        //public DataTable SelectBuscarCliente(string texto)
        //{
        //    return dal.SelectBuscarCliente( texto);
        //}

        }
    }
