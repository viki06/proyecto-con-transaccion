﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using DAL;
using System.Data;
using System.Dynamic;

namespace FARMAVITALY.Venta
{
    /// <summary>
    /// Lógica de interacción para vwVenta.xaml
    /// </summary>
    public partial class vwVenta : Window
    {
        ProductoBRL brlProducto;
        Producto.vwBuscarProducto vwBuscarProduco;
        Common.cliente client;
        ClienteBRL brl;
        UsuarioBRL brlUsuario;
        producto product;
        venta vent;
        ventaBRL brlVenta;
        ProductoVenta productVent;
        DataTable dt;
        List<detalleVenta> detalle;
        Validaciones obj = new Validaciones();
        Methods methods;
        public venta Vent { get => vent; set => vent = value; }
        public vwVenta()
        {
            InitializeComponent();
        }

        public vwVenta(DataTable dt, producto product, venta vent)
        {
            InitializeComponent();
            this.product = product;
            this.dt = dt;
            this.vent = vent;
        }

        public vwVenta(Producto.vwBuscarProducto vwBuscarProduco)
        {
            InitializeComponent();
            this.vwBuscarProduco = vwBuscarProduco;
        }
        void BuscarClienteNit()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNit.Text.Trim()) == false)
                {
                    methods = new Methods();
                    string cmd = string.Format("SELECT razonSocial FROM cliente  WHERE  nit='{0}'", txtNit.Text.Trim());
                    DataSet ds = methods.ejecutar(cmd);
                    txtRazonSocial.Text = ds.Tables[0].Rows[0]["razonSocial"].ToString().Trim();
                }
            }
            catch (Exception)
            {

                MessageBox.Show("No hay datos ");
            }
        }

        public int stock()
        {
            int xStock = 0;
            try
            {
                if (string.IsNullOrEmpty(txtIdProducto.Text.Trim()) == false)
                {
                    methods = new Methods();
                    string cmd = string.Format("SELECT stock FROM producto  WHERE  idProducto='{0}'", txtIdProducto.Text.Trim());
                    DataSet ds = methods.ejecutar(cmd);
                    MessageBox.Show((ds.Tables[0].Rows[0]["stock"].ToString().Trim() + " Productos le Quedan "));
                }
                return xStock;
            }
            catch (Exception)
            {
                MessageBox.Show("No hay datos ");
                return -1;
            }
        }       
        private void BtnRegistroProducto_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwBuscarProducto vw = new Producto.vwBuscarProducto();
            vw.ShowDialog();          
        }
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           //if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count >0)
           // {
           //     ProductoVenta PV = new ProductoVenta();
           //     try
           //     {
           //         PV = (ProductoVenta)DgvDatos.SelectedItem;
           //         txtStock.Text = " " + PV.Stock;
           //         txtNombreProducto.Text = " " + PV.Nombre;
           //         txtPresentacion.Text = " " + PV.Presentacion;
           //         txtPrecioVenta.Text = " " + PV.PreVenta;
           //         txtCantidad.Text = " " + PV.CantProducto;
           //     }
           //     catch (Exception ex)
           //     {
           //         MessageBox.Show("no funciona " + ex.Message);
           //     }

           // }
        }
        void CleartxtPagos()
        {
            txtTotalDescuento.Text = "";
            txtTotalImporte.Text = "";
        }
       void     FillIdAutoencremtVenta()
        {
            try
            {
                brlVenta = new ventaBRL();
                DgvIdAutoIncremetVenta.ItemsSource = null;
                DgvIdAutoIncremetVenta.ItemsSource = brlVenta.SelectAutoIncrementIdVenta().DefaultView;
                DgvIdAutoIncremetVenta.Columns[0].Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
         private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillIdAutoencremtVenta();
             lblUsuario.Content ="Atendido Por :"+Sesion.rolSesion;
       }
        private void BtnVender_Click(object sender, RoutedEventArgs e)
        {            
            if (DgvDatos.Items.Count > 0)
            {
                try
                {
                    if (MessageBox.Show("Esta Segura@ De Realizar la venta..??","Vender", MessageBoxButton.YesNo,
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Vent = new venta(decimal.Parse(txtTotalImporte.Text), 0, Sesion.idSesion, client.IdCliente);
                        detalle = new List<detalleVenta>();  
                        TextBlock precioVenta, cantidad, idProducto;
                        for (int i = 0; i < DgvDatos.Items.Count; i++)  
                        {
                            precioVenta = DgvDatos.Columns[6].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            cantidad = DgvDatos.Columns[7].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            //idProducto = DgvDatos.Columns[3].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            detalleVenta det = new detalleVenta(decimal.Parse(precioVenta.Text), int.Parse(cantidad.Text), int.Parse
                                (txtIdProducto.Text), Vent.IdVenta);
                            detalle.Add(det);
                        }
                    }
                    
                    brlVenta = new ventaBRL(Vent, detalle);
                    brlVenta.Insert();

                    MessageBox.Show("La Venta Realizado Con Exito ");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }

            else
            {
                MessageBox.Show("Debe requerir un Medicamento");
            }
        }
        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                client = new Common.cliente(txtNit.Text, txtRazonSocial.Text);
                brl = new ClienteBRL(client);
                brl.Insert();
                MessageBox.Show("cliente insertado con exito...");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al insertar los datos del cliente" + ex.Message);
            }
        }

        private void TxtBuscarCliente_Click(object sender, RoutedEventArgs e)
        {
            Cliente.vwCliente vw = new Cliente.vwCliente();
            vw.ShowDialog();

            txtNombreProducto.Focus();
        }
        private void TxtBuscarProducto_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwBuscarProducto vw = new Producto.vwBuscarProducto();
            vw.ShowDialog();
            txtCantidad.Focus();
        }      
        int contFila = 0;
        void FillDataGrid()
        {
            try
            {
                brlProducto = new ProductoBRL();
                
                DgvDatos.ItemsSource = brlProducto.SelectStockProducto(int.Parse(txtCantidad.Text)).DefaultView;
                 DgvDatos.Columns[3].Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
      
        private void TxtAgreagr_Click(object sender, RoutedEventArgs e)
        {
            if (contFila == 0)
            {
              if (int.Parse(txtCantidad.Text) > int.Parse(txtStock.Text))
                {
                    MessageBox.Show("no hay cantidad suficiente");
                }
                else
                {
                    try
                    {
                        ProductoVenta item = new ProductoVenta();
                        item.idUsuario = Sesion.idSesion;
                        item.idCliente = int.Parse(txtIdCliente.Text);
                        item.idProducto = int.Parse(txtIdProducto.Text);
                        item.Nombre = txtNombreProducto.Text;
                        item.Presentacion = txtPresentacion.Text;
                        item.PreVenta = decimal.Parse(txtPrecioVenta.Text);
                        item.CantProducto = int.Parse(txtCantidad.Text);
                        item.importe = item.PreVenta * item.CantProducto;

                        DgvDatos.Items.Add(item);
                        DgvDatos.UpdateLayout();
                        //total += item.importe;
                        //txtTotalImporte.Text = total.ToString();
                        decimal aux = calcularTotal();
                        //decimal Des = DescuentoTotal();
                        totalLetras();
                        clearTextProducto();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }

            }
        }
        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DgvDatos.SelectedIndex >= 0)
                {
                    if (MessageBox.Show("Esta Segura@ De Eliminar Esta Fila..??", "ELIMINAR", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        for (int i = 0; i <= DgvDatos.SelectedItems.Count; i++)
                        {
                            DgvDatos.Items.Remove(DgvDatos.SelectedItems[i]);
                            decimal aux = calcularTotal();
                            totalLetras();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Debe Seleccionar una fila se desea Eliminar");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void DgvDatos_Loaded(object sender, RoutedEventArgs e)
        {         

        }   
        void datosCliente()
        {
            client = null;
            if (Generica.idClient!=0)
            {
                client = new cliente(Generica.idClient, Generica.nit, Generica.razonSocial);
                txtIdCliente.Text = client.IdCliente.ToString();
                txtNit.Text = client.Nit;
                txtRazonSocial.Text = client.RazonSocial;
            }
        }
        void ClearCliente()
        {
            txtNit.Text = "";
            txtRazonSocial.Text = "";
        }

        void clearTextProducto()
        {
            txtNombreProducto.Text = "";
            txtPresentacion.Text = "";
            txtStock.Text = "";
            txtPrecioVenta.Text = "";
            txtCantidad.Text = "";
        }
        void datosProducto()
        {
            product = null;
            if (Generica.idProduct != 0)
            {
            product = new producto(Generica.idProduct, Generica.nombreProduct,Generica.stock, Generica.precioVent, Generica.presentation);

                txtIdProducto.Text = product.IdProducto.ToString();
                txtNombreProducto.Text = product.NombreProducto;
                txtStock.Text = product.Stock.ToString();
                txtPresentacion.Text = product.Presentacion;
                txtPrecioVenta.Text = product.PrecioVenta.ToString();
            }
        }
        private void Window_Activated(object sender, EventArgs e)
        {
            datosCliente();
            datosProducto();
        }
        private void TxtNit_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarClienteNit();
                txtNombreProducto.Focus();
            }
        }
       
        public decimal calcularTotal()
        {
            try
            {
                decimal sum = 0;
                TextBlock x;
                for (int i = 0; i < DgvDatos.Items.Count; i++)
                {
                    x = DgvDatos.Columns[8].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                    sum += decimal.Parse(x.Text);
                    txtTotalImporte.Text =sum.ToString();
                }
                return sum;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return -1;
            }
        }
        private void TxtCantidad_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarNumeros(e);
        }

        private void BtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            FillIdAutoencremtVenta();
            clearTextProducto();
            ClearCliente();
            DgvDatos.Items.Clear();
            CleartxtPagos();

        }
        conversion c = new conversion();
        void totalLetras()
        {
            txtTotalDescuento.Text = c.enletras(txtTotalImporte.Text).ToLower();
        }
        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            //if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            //{
            //  try
            //    {
            //        ProductoVenta PV = new ProductoVenta();

            //        PV.Nombre = txtNombreProducto.Text;
            //        PV.Presentacion = txtPresentacion.Text;
            //        PV.Stock = int.Parse(txtStock.Text);
            //        PV.PreVenta = int.Parse(txtPrecioVenta.Text);
            //        PV.CantProducto = int.Parse(txtCantidad.Text.ToString());

            //        DgvDatos.SelectedItem = PV;
                    
            //       MessageBox.Show("Modificado  correctamente:");
            //       }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("no funciona :" + ex.Message);
            //    }

            //}
        }
    }
 }

