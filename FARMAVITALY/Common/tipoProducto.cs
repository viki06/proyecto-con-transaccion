﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class tipoProducto
    {
        #region atributos de la clase con su methods set y get
        private int idTipoProducto ;
        private string nombre ;
        private byte estado ;
        private DateTime fechaActualizacion ;

        public int IdTipoProducto { get => idTipoProducto; set => idTipoProducto = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        #endregion
        #region sobre carga de constructor
        /// <summary>
        /// constructor completo para get
        /// </summary>
        /// <param name="idTipoProducto"></param>
        /// <param name="nombre"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        public tipoProducto(int idTipoProducto ,  string nombre ,     byte estado ,                DateTime fechaActualizacion    )
        {
            this.idTipoProducto = idTipoProducto;
            this.nombre = nombre;
            this.estado = estado;
            this.fechaActualizacion = fechaActualizacion;
        }
        /// <summary>
        /// constructor para el insert
        /// </summary>
        /// <param name="nombre"></param>
        public tipoProducto( string nombre)
        {
                            this.nombre = nombre;
           
        }

        public tipoProducto()
        {

        }
        #endregion

    }
}
