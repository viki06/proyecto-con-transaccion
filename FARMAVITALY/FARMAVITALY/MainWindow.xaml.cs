﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Common;

namespace FARMAVITALY
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnUsuario_Click_1(object sender, RoutedEventArgs e)
        {
            Usuario.vwUsuario vw = new Usuario.vwUsuario();
            vw.ShowDialog();
        }

        private void BtnProveedor_Click(object sender, RoutedEventArgs e)
        {
            Proveedor.vwProveedor vw = new Proveedor.vwProveedor();
            vw.ShowDialog();
        }

     

        private void Btnventa_Click(object sender, RoutedEventArgs e)
        {
            Venta.vwVenta vw = new Venta.vwVenta();
            vw.ShowDialog();

        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login.vwLogin vw = new Login.vwLogin();
            vw.ShowDialog();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }       
    
        private void BtnUsuario_Click(object sender, RoutedEventArgs e)
        {
            Usuario.vwUsuario vw = new Usuario.vwUsuario();
            vw.ShowDialog();
        }

      
        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {

            ButtonCloseMenu.Visibility = Visibility.Hidden;
            ButtonOpenMenu.Visibility = Visibility.Visible;


        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {

            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Hidden;
        }

         private void BtnOcultar_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void BtnMaximizar_Click_1(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }

        private void BtnMenimizar_Click_1(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
        }
         private void BtnCateg_Click(object sender, RoutedEventArgs e)
        {
            Categoria.vwCategoria vw = new Categoria.vwCategoria();
            vw.ShowDialog();
        }
        private void BtnTipoProduct_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwTipoProducto vw = new Producto.vwTipoProducto();
            vw.ShowDialog();
        }

        private void BtnProduct_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwProducto vw = new Producto.vwProducto();
            vw.ShowDialog();
        }

        private void BtnClient_Click(object sender, RoutedEventArgs e)
        {
            Cliente.vwCliente vw = new Cliente.vwCliente();
            vw.ShowDialog();
           
        }

        private void BtnMovimiento_Click(object sender, RoutedEventArgs e)
        {
            Movimiento.vwMovimiento vw = new Movimiento.vwMovimiento();
            vw.ShowDialog();
        }


        private void BtnProveed_Click(object sender, RoutedEventArgs e)
        {
            Proveedor.vwProveedor vw = new Proveedor.vwProveedor();
            vw.ShowDialog();
        }
        private void BtnVent_Click(object sender, RoutedEventArgs e)
        {
            Venta.vwVenta vw = new Venta.vwVenta();
            vw.ShowDialog();
        }

      
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsuario.Text = "rol : " + Sesion.usuarioSesion + "usuario  :" + Sesion.rolSesion;

            switch (Sesion.rolSesion)
            {
                case "farmaceutico":
                    listViewItem3.Visibility = Visibility.Hidden;
                    break;
                case"contador":
                    listViewItem1.Visibility = Visibility.Hidden;
                    //listViewItem.Visibility = Visibility.Hidden;
                    //listViewItem2.Visibility = Visibility.Hidden;
                    break;
            }
        }
        private void BtnListaProducto_Click(object sender, RoutedEventArgs e)
        {
            Producto.vwBuscarProducto vw = new Producto.vwBuscarProducto();
            vw.ShowDialog();
        }

        private void BtnReporte_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}


   

