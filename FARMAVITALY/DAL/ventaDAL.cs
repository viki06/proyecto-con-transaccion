﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class ventaDAL : AbstractDAL
    {
        cliente cliente;
      private  venta vent;
      private   List<detalleVenta> detalle;
        public venta Vent { get => vent; set => vent = value; }
        public List<detalleVenta> Detalle { get => detalle; set => detalle = value; }

        public ventaDAL(venta vent, List<detalleVenta> detalle)
        {
            this.vent = vent;
            this.detalle=detalle;
        }
        
        public ventaDAL()
        {

        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }
        public override void Insert()
        {
            string queryVenta = @"INSERT INTO venta(totalPago,descuento,idUsuario,idCliente)
                                           VALUES (@totalPago,@descuento,@idUsuario,@idCliente)";

            string queryDetalleVenta = @"INSERT INTO detalleventa(precioVenta,cantidad,idProducto,idVenta)
                                                         VALUES (@precioVenta,@cantidad,@idProducto,@idVenta)";

            List<MySqlCommand> cmds = new List<MySqlCommand>();

            try
            {
                cmds = Methods.CreateNBasicCommand(1 + detalle.Count);
                cmds[0].CommandText = queryVenta;
                cmds[0].Parameters.AddWithValue("@totalPago", vent.TotalPago);
                cmds[0].Parameters.AddWithValue("@descuento", vent.Descuento);
                cmds[0].Parameters.AddWithValue("@idUsuario", Sesion.idSesion);
                cmds[0].Parameters.AddWithValue("@idCliente", vent.IdCliente);

                int id = GetAutoincrementIdVenta();

                for (int i = 1; i < cmds.Count; i++)
                {
                    cmds[i].CommandText = queryDetalleVenta;
                    cmds[i].Parameters.AddWithValue("@precioVenta", detalle[i - 1].PrecioVenta);
                    cmds[i].Parameters.AddWithValue("@cantidad", detalle[i - 1].Cantidad);
                    cmds[i].Parameters.AddWithValue("@idProducto", detalle[i - 1].IdProducto);
                    cmds[i].Parameters.AddWithValue("@idVenta", id);
                }
                Methods.ExecuteNBasicComandTran(cmds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void InsertMerma()
        //{
        //    string queryVenta = @"INSERT INTO venta(totalPago,descuento,idUsuario,idCliente)                     
        //                               VALUES (@totalPago,@descuento,@idUsuario,@idCliente)"; 
               
        //string queryDetalleVenta = @"INSERT INTO detalleventa(precioVenta,cantidad,idProducto,idVenta)
        //                                                 VALUES (@precioVenta,@cantidad,@idProducto,@idVenta)";

        //    List<MySqlCommand> cmds = new List<MySqlCommand>();

        //    try
        //    {
        //        cmds = Methods.CreateNBasicCommand(1 + detalle.Count);
        //        cmds[0].CommandText = queryVenta;
        //        cmds[0].Parameters.AddWithValue("@totalPago", vent.TotalPago);
        //        cmds[0].Parameters.AddWithValue("@descuento", vent.Descuento);
        //        cmds[0].Parameters.AddWithValue("@idUsuario", Sesion.idSesion);
        //        cmds[0].Parameters.AddWithValue("@idCliente", vent.IdCliente);

        //          int id = GetAutoincrementIdVenta();

        //          for (int i = 1; i < cmds.Count; i++)
        //          {
        //            cmds[i].CommandText = queryDetalleVenta;
        //            cmds[i].Parameters.AddWithValue("@precioVenta",detalle[i-1].PrecioVenta);
        //            cmds[i].Parameters.AddWithValue("@cantidad", detalle[i-1].Cantidad);
        //            cmds[i].Parameters.AddWithValue("@idProducto", detalle[i-1].IdProducto);
        //            cmds[i].Parameters.AddWithValue("@idVenta",id);

        //          }
        //          Methods.ExecuteNBasicComandTran(cmds);
        //      }
        //      catch (Exception ex)
        //      {

        //          throw ex;
        //      }
        //  }



        public int GetAutoincrementIdVenta()
        {
            return Methods.GetAutoIncrementTable("venta");
        }
        public venta Get(int idVenta)
        {
            venta res = null;

            string query = @"SELECT idVenta,totalPago,descuento,estado,fechaActualizacion,idUsuario,idCliente
                              FROM venta
                              where  idVenta=@id";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idVenta);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new venta(int.Parse(dr[0].ToString()),decimal.Parse(dr[1].ToString()), decimal.Parse(dr[2].ToString()), byte.Parse(dr[3].ToString()),DateTime.Parse(dr[4].ToString()), int.Parse(dr[5].ToString()), int.Parse(dr[6].ToString()));
                }

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }
        public override DataTable Select()
        {

            DataTable res = new DataTable();
            string query = @"SELECT idVenta,totalPago,descuento 
                             FROM venta
                             WHERE estado=1 idCliente=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;        
    }
        public DataTable SelectAutoIncrementIdVenta()
        {

            DataTable res = new DataTable();
            string query = @"select AUTO_INCREMENT AS 'Nro De Venta'
                        from information_schema.tables 
                       Where table_schema ='bd' 
                       and table_name = 'venta'";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        public  DataTable SelectVenta()
        {

            DataTable res = new DataTable();
            string query = @"SELECT idVenta,totalPago,descuento 
                             FROM venta
                             WHERE estado=1 AND idCliente=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", cliente.IdCliente);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        public override void Update()
        {
            throw new NotImplementedException();
        }

    }
}
