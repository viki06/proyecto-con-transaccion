﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace FARMAVITALY.Producto
{
    /// <summary>
    /// Lógica de interacción para vwBuscarProducto.xaml
    /// </summary>
    public partial class vwBuscarProducto : System.Windows.Window
    {
        Producto.vwBuscarProducto buscarProduct;

        Validaciones obj = new Validaciones();
    Common.producto prod;
        
         ProductoBRL brlProducto;
        Excel.DataTable dt;
     
        public vwBuscarProducto()
        {
            InitializeComponent();
        }
        public vwBuscarProducto(Producto.vwBuscarProducto buscarProduct)
        {
            InitializeComponent();
            this.buscarProduct = buscarProduct;
        }

        void FillDataGrid()
        {
            try
            {
                brlProducto = new ProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brlProducto.ListaProducto().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        void BuscarProducto()
        {
            try
            {

                brlProducto = new ProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource= brlProducto.BuscarProductoNombre(txtBuscarProducto.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }       
         private void BtnElegirProduct_Click(object sender, RoutedEventArgs e)
         {

            if (prod != null)
            {
                Generica.idProduct = prod.IdProducto;
                Generica.nombreProduct = prod.NombreProducto;
                Generica.precioVent = prod.PrecioVenta;
                Generica.presentation = prod.Presentacion;
                Generica.stock = prod.Stock;


                this.Close();
            }
            else
            {
                MessageBox.Show("Debe selecionar un Producto");
            }

        }
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            prod = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brlProducto = new ProductoBRL();
                    prod = brlProducto.Get(id);
                   
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error al hacer un select " + ex.Message);
                }

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
        }
         private void TxtBuscarProducto_TextChanged(object sender, TextChangedEventArgs e)
        {         
            if (txtBuscarProducto.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscarProducto.Text.Length >= 3)
                {
                    BuscarProducto();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
             }
        }

        private void TxtBuscarProducto_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }
      
        private void MiSelectProducto_Click(object sender, RoutedEventArgs e)
        {

            if (prod != null)
            {
                Generica.idProduct = prod.IdProducto;
                Generica.nombreProduct = prod.NombreProducto;
                Generica.precioVent = prod.PrecioVenta;
                Generica.presentation = prod.Presentacion;
                
                this.Close();
            }
            else
            {
                MessageBox.Show("Debe selecionar un Producto");
            }
        }

        private void BtnExcel_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application excel = new Excel.Application();
          
            excel.Visible = true;
         
            Workbook workbook = excel.Workbooks.Add(System.Reflection.Missing.Value);
            Worksheet sheet1 = (Worksheet)workbook.Sheets[1];
           
            try
            {
                for (int j = 1; j < DgvDatos.Columns.Count; j++)
                {
                    Range myRange = (Range)sheet1.Cells[j];                    
                    sheet1.Cells[j].Interior.ColorIndex =14;
                    sheet1.Cells[j].Font.Bold = 12;
                    sheet1.Columns[j].ColumnWidth = 15;
                 
                    myRange.Value2 = DgvDatos.Columns[j].Header;                  
                        
                }
                for (int j = 0; j < DgvDatos.Items.Count; j++)
                {
                    for (int i = 1; i < DgvDatos.Columns.Count; i++)
                    {
                        Range myRange = (Range)sheet1.Cells[j+2,i];
                        myRange.Value2 = ((DataRowView)DgvDatos.Items[j]).Row[i].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
           
        }
    }
}

