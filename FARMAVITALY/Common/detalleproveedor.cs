﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public   class detalleproveedor
    {
        #region atributos con su methods set y get
        private int idProducto;
        private int idProveedor;

        public int IdProducto { get => idProducto; set => idProducto = value; }
        public int IdProveedor { get => idProveedor; set => idProveedor = value; }

        #endregion

        #region sobre carga de constructor
        /// <summary>
        /// constructor completo
        /// </summary>
        /// <param name="idProducto"></param>
        /// <param name="idProveedor"></param>
        public detalleproveedor(int idProducto,int idProveedor    )
        {
            this.idProducto = idProducto;
           this.idProveedor = idProveedor;
        }
        public detalleproveedor(int idProducto)
        {
            this.idProducto = idProducto;
           
        }
        #endregion

    }
}
