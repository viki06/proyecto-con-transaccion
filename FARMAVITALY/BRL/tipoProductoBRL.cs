﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class tipoProductoBRL : AbstractBRL
    {
      private   tipoProducto tipoProduct;
      private   tipoProductoDAL dal;

        public tipoProducto TipoProduct { get => tipoProduct; set => tipoProduct = value; }
        public tipoProductoDAL Dal { get => dal; set => dal = value; }

        public tipoProductoBRL()
        {
            dal = new tipoProductoDAL();
        }
        public tipoProductoBRL ( tipoProducto tipoProduct)
        {
            this.tipoProduct = tipoProduct;
            dal = new tipoProductoDAL (this.tipoProduct);
        }
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }
        public  DataTable BuscarTipoProducto(string texto)
        {
            return dal.BuscarTipoProducto(texto);
        }

        public override void Update()
        {
            dal.Update();
        }
        public DataTable SelectIDName()
        {
            dal = new tipoProductoDAL();
            return dal.SelectIDName();
        }


        public tipoProducto Get(int idTipoProducto)
        {
            return dal.Get(idTipoProducto);
        }

        
    }
}
