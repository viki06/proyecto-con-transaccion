﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class detalleVenta
    {
        #region atributos de la clase con su methods set y get
        
        private decimal precioVenta;
        private int cantidad;
        private int     idProducto;
        private int   idVenta;


        public decimal PrecioVenta { get => precioVenta; set => precioVenta = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }
        public int IdVenta { get => idVenta; set => idVenta = value; }
        #endregion


        #region sobre carga de constructor

        public detalleVenta(  decimal precioVenta, int cantidad, int idProducto,int idVenta)
        {
            this.precioVenta = precioVenta;
            this.cantidad = cantidad;
            this. idProducto= idProducto;
            this.idVenta=     idVenta;
          }
      
        public detalleVenta()
        {

        }
        #endregion
    }
}
