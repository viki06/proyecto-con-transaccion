﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class venta
    {
        #region atributos de la clase con su methods set y get
private int             idVenta;
private  decimal        totalPago  ;
private  decimal         descuento         ;
private  byte           estado               ;
private DateTime        fechaActualizacion;
        private int      idUsuario;
private  int            idCliente;

        public int IdVenta { get => idVenta; set => idVenta = value; }
        public decimal TotalPago { get => totalPago; set => totalPago = value; }
        public decimal Descuento { get => descuento; set => descuento = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public int IdCliente { get => idCliente; set => idCliente = value; }

        #endregion
        #region sobre carga de constructor
        /// <summary>
        /// constrictor para get
        /// </summary>
        /// <param name="idVenta"></param>
        /// <param name="totalPago"></param>
        /// <param name="descuento"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idCliente"></param>
        public venta(int idVenta, decimal totalPago , decimal descuento  ,    byte estado,     DateTime fechaActualizacion,    int idUsuario,  int idCliente)

        {
            this. idVenta= idVenta;
            this. totalPago  = totalPago  ;
            this.descuento = descuento;
            this.estado = estado;
            this.fechaActualizacion = fechaActualizacion;
            this.  idUsuario=  idUsuario;
            this.idCliente= idCliente;

        }
        /// <summary>
        /// numero de venta
        /// </summary>
        /// <param name="idVenta"></param>
        public venta(int idVenta)
        {
            this.idVenta = idVenta;
         }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="totalPago"></param>
        /// <param name="descuento"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idCliente"></param>
        public venta(decimal totalPago, decimal descuento, int idUsuario, int idCliente)

        {
            this.totalPago = totalPago;
            this.descuento = descuento;
            this.idUsuario = idUsuario;
            this.idCliente = idCliente;
        }

        public venta()
        {

        }
        #endregion

    }
}
