﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class ProveedorBRL : AbstractBRL
    {
      private  proveedor proveed;
        private List<detalleproveedor> detalle;
        private  ProveedorDAL dal;

        public proveedor Proveed { get => proveed; set => proveed = value; }
        public ProveedorDAL Dal { get => dal; set => dal = value; }
        public List<detalleproveedor> Detalle { get => detalle; set => detalle = value; }

        public ProveedorBRL(proveedor proveed, List<detalleproveedor> detalle)
        {
            this.proveed = proveed;
            this.detalle = detalle;
            dal = new ProveedorDAL(this.proveed,this.detalle);
        }
        public ProveedorBRL(proveedor proveed)
        {
            this.proveed = proveed;
            dal = new ProveedorDAL(this.proveed);
        }
        public ProveedorBRL()
        {
            dal = new ProveedorDAL();
        }
        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }
        public DataTable SelectIDNameProveedor()
        {
            dal = new ProveedorDAL();
            return dal.SelectIDNameProveedor();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }

        public override void Update()
        {
            dal.Update();
        }
        public proveedor Get(int idProveedor)
        {
            return dal.Get(idProveedor);
        }
        public DataTable SelectBuscarProveedor(string texto)
        {
            return dal.SelectBuscarProveedor(texto);
        }

       
    }
}
