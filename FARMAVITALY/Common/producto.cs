﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class producto
    {


        #region atributos de la clase Medicamento
        private int idProducto;
        private string      nombreProducto;
        private decimal      precioCompra;
        private decimal      precioVenta;      
        private string  nroLote;
        private DateTime fechaCaducidad;
        private int stock;      
         private string       presentacion;
         private  byte estado ;
        private DateTime     fechaActualizacion;
        private int         idTipoProducto;
        private int         idCategoria;

        public int IdProducto { get => idProducto; set => idProducto = value; }
        public string NombreProducto { get => nombreProducto; set => nombreProducto = value; }
        public decimal PrecioCompra { get => precioCompra; set => precioCompra = value; }
        public decimal PrecioVenta { get => precioVenta; set => precioVenta = value; }
        public int Stock { get => stock; set => stock = value; }
        public string Presentacion { get => presentacion; set => presentacion = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        public int IdTipoProducto { get => idTipoProducto; set => idTipoProducto = value; }
        public int IdCategoria { get => idCategoria; set => idCategoria = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaCaducidad { get => fechaCaducidad; set => fechaCaducidad = value; }
        public string NroLote { get => nroLote; set => nroLote = value; }

        #endregion

        #region sobre carga de constructor
       /// <summary>
       /// constructor para get
       /// </summary>
       /// <param name="idProducto"></param>
       /// <param name="nombreProducto"></param>
       /// <param name="precioCompra"></param>
       /// <param name="precioVenta"></param>
       /// <param name="nroLote"></param>
       /// <param name="fechaCaducidad"></param>
       /// <param name="stock"></param>
       /// <param name="presentacion"></param>
       /// <param name="estado"></param>
       /// <param name="fechaActualizacion"></param>
       /// <param name="idTipoProducto"></param>
       /// <param name="idCategoria"></param>
        public producto(int idProducto, string nombreProducto,decimal precioCompra, decimal precioVenta, string nroLote, DateTime fechaCaducidad,int stock,   string presentacion,   byte estado,   DateTime fechaActualizacion,int idTipoProducto,int idCategoria)
            {
            this.idProducto = idProducto;
            this.   nombreProducto=   nombreProducto;
            this.    precioCompra=    precioCompra;
            this.    precioVenta=    precioVenta;
            this.nroLote = nroLote;
            this.fechaCaducidad = fechaCaducidad;
            this.    stock=    stock;
            this.    presentacion=    presentacion;
             this.estado = estado;
            this.fechaActualizacion = fechaActualizacion;
            this.   idTipoProducto=   idTipoProducto;
            this.idCategoria=   idCategoria;

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="nombreProducto"></param>
        /// <param name="precioCompra"></param>
        /// <param name="precioVenta"></param>
        /// <param name="nroLote"></param>
        /// <param name="fechaCaducidad"></param>
        /// <param name="stock"></param>
        /// <param name="presentacion"></param>
        /// <param name="idTipoProducto"></param>
        /// <param name="idCategoria"></param>
        public producto(string nombreProducto, decimal precioCompra, decimal precioVenta, string nroLote, DateTime fechaCaducidad, int stock, string presentacion,  int idTipoProducto, int idCategoria)
        {            
            this.nombreProducto = nombreProducto;
            this.precioCompra = precioCompra;
            this.precioVenta = precioVenta;
            this.nroLote = nroLote;
            this.fechaCaducidad = fechaCaducidad;
            this.stock = stock;
            this.presentacion = presentacion;
            this.idTipoProducto = idTipoProducto;
            this.idCategoria = idCategoria;


        }

        /// <summary>
        /// insert para selectProducto
        /// </summary>
        /// <param name="idProducto"></param>
        /// <param name="nombreProducto"></param>
        /// <param name="precioVenta"></param>
        /// <param name="stock"></param>
        /// <param name="presentacion"></param>
        /// <param name="fechaActualizacion"></param>
        public producto(int idProducto, string nombreProducto,  decimal precioVenta, DateTime fechaCaducidad, int stock, string presentacion,  DateTime fechaActualizacion)
        {
            this.idProducto = idProducto;
            this.nombreProducto = nombreProducto;          
            this.precioVenta = precioVenta;
            this.fechaCaducidad = fechaCaducidad;
            this.stock = stock;
            this.presentacion = presentacion;
            this.fechaActualizacion = fechaActualizacion;        }

        /// <summary>
        /// CONSTRUCTOR INSERTAR
        /// </summary>
        /// <param name="nombreProducto"></param>
        /// <param name="precioCompra"></param>
        /// <param name="precioVenta"></param>
        /// <param name="stock"></param>
        /// <param name="presentacion"></param>
        public producto(string nombreProducto, decimal precioCompra, decimal precioVenta, string nroLote, DateTime fechaCaducidad, int stock,  string presentacion)
        {

            this.nombreProducto = nombreProducto;
            this.precioCompra = precioCompra;
            this.precioVenta = precioVenta;
            this.nroLote = nroLote;
            this.fechaCaducidad = fechaCaducidad;
            this.stock = stock;
            this.presentacion = presentacion;
        }
        /// <summary>
        /// envio de producto
        /// </summary>
        /// <param name="idProducto"></param>
        /// <param name="nombreProducto"></param>
        /// <param name="stock"></param>
        /// <param name="precioVenta"></param>
        /// <param name="presentacion"></param>
        public producto(int idProducto,string nombreProducto, int stock, decimal precioVenta,  string presentacion)
        {
            this.idProducto = idProducto;
            this.nombreProducto = nombreProducto;
            this.stock = stock;
            this.precioVenta = precioVenta;
            this.presentacion = presentacion;        
         }
        /// <summary>
        /// id para detalle proveedor
        /// </summary>
        /// <param name="idProducto"></param>
        public producto(int idProducto)
        {
            this.idProducto = idProducto;
        }
        public producto()
        {

        }

       
        #endregion
    }
}






