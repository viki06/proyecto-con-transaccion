﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace FARMAVITALY.Usuario
{
    /// <summary>
    /// Lógica de interacción para vwUsuario.xaml
    /// </summary>
    public partial class vwUsuario : System.Windows.Window
    {
        public vwUsuario()
        {
            InitializeComponent();
        }

        byte opcion = 0;
        Common.Usuario user;
        UsuarioBRL brl;
        Validaciones obj = new Validaciones();

        void Habilitar(byte opcion)
        {
            this.opcion = opcion;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnElimiar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;
            txtnombreUser.Focus();
        }
        void DesHabilitar()
        {

            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnElimiar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;

        }

        void ClearTxtUsuario()
        {
            txtnombreUser.Text = "";
            txtprimerAp.Text = "";
            txtSegundoAp.Text = "";
            txtTelefono.Text = "";
            txtCi.Text = "";
            txtDireccion.Text = "";
        }
        void FillDataGrid()
        {
            try
            {
                brl = new UsuarioBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + " error al filtrar");
            }
        }
        void FillBuscarUsuario()
        {
            try
            {
                brl = new UsuarioBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.BuscarUsuarioNombrePrimerAp(txtBuscarUsuario.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + " error al filtrar");
            }
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {

            switch (opcion)
            {
                case 1:

                    try
                    {

                        user = new Common.Usuario(txtnombreUser.Text, txtprimerAp.Text, txtSegundoAp.Text, txtCi.Text, int.Parse(txtTelefono.Text), txtDireccion.Text, DateTime.Parse(dtFechaNac.ToString()), cbxRol.Text);
                        brl = new UsuarioBRL(user);
                        brl.Insert();
                        MessageBox.Show("Proveedor insertado con exito...");
                        ClearTxtUsuario();
                        FillDataGrid();
                        DesHabilitar();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("error al insert  " + ex.Message);
                    }
                    break;

                case 2:
                    try
                    {
                        user.Nombre = txtnombreUser.Text;
                        user.PrimerApellido = txtprimerAp.Text;
                        user.SegundoApellido = txtSegundoAp.Text;
                        user.Ci = txtCi.Text;
                        user.Telefono = int.Parse(txtTelefono.Text);
                        user.Direccion = txtDireccion.Text;
                        user.Rol = cbxRol.Text;
                        user.FechaNacimiento =DateTime.Parse( dtFechaNac.Text);
                        brl.Update();
                        MessageBox.Show("Los Datos del Proveedor Fueron Modificados con exito..");
                        ClearTxtUsuario();
                        FillDataGrid();

                        DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar Datos del Proveedor" + ex.Message);
                    }
                    break;
            }
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void BtnElimiar_Click(object sender, RoutedEventArgs e)
        {

            if (DgvDatos.SelectedItem != null && user != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new UsuarioBRL(user);
                        brl.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();

        }


        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            user = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new UsuarioBRL();
                    user = brl.Get(id);
                    if (user != null)
                    {
                        txtnombreUser.Text = user.Nombre;
                        txtprimerAp.Text = user.PrimerApellido;
                        txtSegundoAp.Text = user.SegundoApellido;
                        txtCi.Text = user.Ci;
                        txtDireccion.Text = user.Direccion;
                        txtTelefono.Text = user.Telefono.ToString();

                        // dtFechaNac.Text= DateTime.Now.ToUniversalTime().AddMinutes(DateTime.Now.Subtract(DateTime.Now.ToUniversalTime()).TotalMinutes).ToString("dd/MM/yyyy");
                        dtFechaNac.Text = user.FechaNacimiento.ToString();
                       // dtFechaNac.Text=user.FechaNacimiento.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        cbxRol.Text = user.Rol;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error en el get " + ex.Message);
                }
            }
        }
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
            FillBuscarUsuario();
        }

        private void TxtnombreUser_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtprimerAp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtSegundoAp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtTelefono_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarNumeros(e);
        }

        private void TxtBuscarUsuario_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscarUsuario.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscarUsuario.Text.Length >= 3)
                {
                    FillBuscarUsuario();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }

        private void TxtBuscarUsuario_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            obj.validarLetras(e);
        }

        private void TxtBuscarUsuario_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                FillBuscarUsuario();
                txtnombreUser.Focus();
            }
        }
      
    private void BtnExcel_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application excel = new Excel.Application();
            excel.Visible = true;
           
            Workbook workbook = excel.Workbooks.Add(System.Reflection.Missing.Value);
            Worksheet sheet1 = (Worksheet)workbook.Sheets[1];
            try
            {
            

                excel.Cells.Borders.ColorIndex=5;
                excel.Cells.Interior.ColorIndex =10;
                              

                for (int j = 1; j < DgvDatos.Columns.Count; j++)
                {
                    Range myRange = (Range)sheet1.Cells[j];

                    sheet1.Cells[j].Font.Bold = 14;
                    sheet1.Cells[j].Interior.ColorIndex = 8;

                  sheet1.Columns[j].ColumnWidth = 15;
                    myRange.Value2 = DgvDatos.Columns[j].Header;
                }
                for (int j = 0; j < DgvDatos.Items.Count; j++)
                {
                    for (int i = 1; i < DgvDatos.Columns.Count; i++)
                    {
                        Range myRange = (Range)sheet1.Cells[j + 2, i];
                        myRange.Value2 = ((DataRowView)DgvDatos.Items[j]).Row[i].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
       
    



