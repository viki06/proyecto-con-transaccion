﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace FARMAVITALY.Cliente
{
    /// <summary>
    /// Lógica de interacción para vwCliente.xaml
    /// </summary>
    public partial class vwCliente : Window
    {
        public vwCliente()
        {
            InitializeComponent();
        }
        byte opcion = 0;
        Common.cliente client;
        ClienteBRL brl;

        void Habilitar(byte op)
        {
            this.opcion = op;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;
            txtNit.Focus();



        }

        void DesHabilitar()
        {

            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;



        }
        void FillDataGrid()
        {
            try
            {
                brl = new ClienteBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {

            if (DgvDatos.SelectedItem != null && client != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new ClienteBRL(client);
                        brl.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {

            switch (opcion)
            {
                case 1:

                    try
                    {
                        client = new Common.cliente(txtNit.Text, txtRazonSocial.Text);
                        brl = new ClienteBRL(client);
                        brl.Insert();
                        MessageBox.Show("cliente insertado con exito...");
                        FillDataGrid();
                        DesHabilitar();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                    break;

                case 2:
                    try
                    {
                        client.Nit = txtNit.Text;
                        client.RazonSocial = txtRazonSocial.Text;
                         brl = new ClienteBRL(client);
                        brl.Update();
                        FillDataGrid();


                        MessageBox.Show(" CLIENTE Modificado con exito...");
                     

                        DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar " + ex.Message);
                    }
                    break;
            }
        }

        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            client = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ClienteBRL();
                    client = brl.Get(id);
                    if (client != null)
                    {

                        //txtNit.Text = client.Nit;
                        //txtRazonSocial.Text = client.RazonSocial;

                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error en el get " + ex.Message);
                }

            }

        }

      
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        void BuscarCliente()
        {
            try
            {
                brl = new ClienteBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.BuscarClienteNit(txtNit.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
         
       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
        }

        private void MIenviar_Click(object sender, RoutedEventArgs e)
        {
            if (client!=null)
            {
                Generica.idClient= client.IdCliente;
                Generica.nit = client.Nit;
                Generica.razonSocial = client.RazonSocial;
                this.Close();
                    
            }
            else
            {
                MessageBox.Show("Debe selecionar un cliente");
            }
        }

        private void BtnSelectCliente_Click(object sender, RoutedEventArgs e)
        {
            if (client!= null)
            {
                Generica.idClient = client.IdCliente;
                Generica.nit = client.Nit;
                Generica.razonSocial = client.RazonSocial;
                this.Close();

            }
            else
            {
                MessageBox.Show("Debe selecionar un cliente");
            }
        }

        private void TxtBuscarCliente_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtNit.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtNit.Text.Length >= 3)
                {
                    BuscarCliente();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }
    }
}
    
