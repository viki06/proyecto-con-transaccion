﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class cliente
    {
        #region atributos de la clase y su methods get y set
        private int      idCliente;
        private string      nit;
        private string   razonSocial;
        private byte    estado;
        private DateTime fechaActualizacion;

        public int IdCliente { get => idCliente; set => idCliente = value; }
        public string Nit { get => nit; set => nit = value; }
        public string RazonSocial { get => razonSocial; set => razonSocial = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }

        #endregion

        #region sobre carga de constructor
        /// <summary>
        /// constructor para get
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="nit"></param>
        /// <param name="razonSocial"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        public cliente(int idCliente, string nit,   string razonSocial, byte estado, DateTime fechaActualizacion       )
        {
            this. idCliente= idCliente;
            this. nit= nit;
            this. razonSocial= razonSocial;
            this.estado=estado;
            this.fechaActualizacion= fechaActualizacion;
           
        }
        /// <summary>
        /// CONSTRUCTOR PARA CARGAR A VENTAS
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="nit"></param>
        /// <param name="razonSocial"></param>
        public cliente(int idCliente, string nit, string razonSocial)
        {
            this.idCliente = idCliente;
            this.nit = nit;
            this.razonSocial = razonSocial;
          
        }
        /// <summary>
        /// constructor para insert
        /// </summary>
        /// <param name="nit"></param>
        /// <param name="razonSocial"></param>
        public cliente(string nit, string razonSocial)
        {
            this.nit = nit;
            this.razonSocial = razonSocial;
           
        }
        public cliente()
        {

        }
        #endregion
    }
}
