﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class proveedor
    {

        #region atributos y mrthods de la clase
private int    idProveedor;
private string nombre;
private string primerApellido    ;
private string segundoApellido   ;
private int telefono          ;
private double latitud           ;
private double longitud          ;
 private string correoElectronico;
private byte   estado            ;
private DateTime fechaActualizacion;

        public int IdProveedor { get => idProveedor; set => idProveedor = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string PrimerApellido { get => primerApellido; set => primerApellido = value; }
        public string SegundoApellido { get => segundoApellido; set => segundoApellido = value; }
        public int Telefono
        {
            get { return telefono; }
            set
            {
                if (value > 60000000)
                {
                    telefono = value;
                }
                else
                {
                    telefono = 0;
                }
            }
        }

        public double Latitud { get => latitud; set => latitud = value; }
        public double Longitud { get => longitud; set => longitud = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        public string CorreoElectronico { get => correoElectronico; set => correoElectronico = value; }

        #endregion

        #region sobre carga de constructor
        /// <summary>
        /// constructor para get
        /// </summary>
        /// <param name="idProveedor"></param>
        /// <param name="nombre"></param>
        /// <param name="primerApellido"></param>
        /// <param name="segundoApellido"></param>
        /// <param name="telefono"></param>
        /// <param name="latitud"></param>
        /// <param name="longitud"></param>
        /// <param name="correoElectronico"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>

        public proveedor(int idProveedor,    string nombre,           string primerApellido    ,             string segundoApellido   ,   int telefono   , double latitud , double longitud , string correoElectronico, byte estado  ,  DateTime fechaActualizacion )
        {
            this.idProveedor= idProveedor;
            this. nombre= nombre;
            this. primerApellido    = primerApellido    ;
            this. segundoApellido   = segundoApellido   ;
            this. telefono          = telefono          ;
            this. latitud           = latitud           ;
            this. longitud          = longitud          ;
            this.correoElectronico = correoElectronico;
            this. estado            = estado            ;
            this.fechaActualizacion = fechaActualizacion;
        }

        public proveedor(int idProveedor)
        {
            this.idProveedor = idProveedor;
           
        }
        public proveedor(int idProveedor, string correoElectronico)
        {
            this.idProveedor = idProveedor;
            this.correoElectronico = correoElectronico;
        }
        /// <summary>
        /// constructor insert
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="primerApellido"></param>
        /// <param name="segundoApellido"></param>
        /// <param name="telefono"></param>
        /// <param name="latitud"></param>
        /// <param name="longitud"></param>
        /// <param name="correoElectronico"></param>
        public proveedor( string nombre, string primerApellido, string segundoApellido, int telefono, double latitud, double longitud, string correoElectronico)
        {
            this.nombre = nombre;
            this.primerApellido = primerApellido;
            this.segundoApellido = segundoApellido;
            this.telefono = telefono;
            this.latitud = latitud;
            this.longitud = longitud;
            this.correoElectronico = correoElectronico;
           }
        public proveedor()
        {

        }
        #endregion

    }
}
