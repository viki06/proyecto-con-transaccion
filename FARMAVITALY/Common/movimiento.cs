﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
  public  class movimiento
    {
        #region atributos de la clase y su methods set y get
        private int     idmovimiento;
        private string tipoMovimiento;
        private string subTipo;
        private int      cantidad;
        private int      nroLote;
        private DateTime fechaCaducidad;
        private decimal precioUnitario;
        private byte    estado;
        private string  fechaActualizacion;
        private int     idProducto;

        public int Idmovimiento { get => idmovimiento; set => idmovimiento = value; }
        public string TipoMovimiento { get => tipoMovimiento; set => tipoMovimiento = value; }
        public string SubTipo { get => subTipo; set => subTipo = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int NroLote { get => nroLote; set => nroLote = value; }
        public DateTime FechaCaducidad { get => fechaCaducidad; set => fechaCaducidad = value; }
        public decimal PrecioUnitario { get => precioUnitario; set => precioUnitario = value; }
        public byte Estado { get => estado; set => estado = value; }
        public string FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }
        public int IdProducto { get => idProducto; set => idProducto = value; }

        #endregion
        #region sobre carga de constructor
        /// <summary>
        /// constructor para el get
        /// </summary>
        /// <param name="idmovimiento"></param>
        /// <param name="tipoMovimiento"></param>
        /// <param name="subTipo"></param>
        /// <param name="cantidad"></param>
        /// <param name="nroLote"></param>
        /// <param name="fechaCaducidad"></param>
        /// <param name="precioUnitario"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        /// <param name="idProducto"></param>
        public movimiento(int idmovimiento,   string tipoMovimiento,  string subTipo,        int cantidad,int nroLote,  DateTime fechaCaducidad,decimal precioUnitario,      byte estado,string fechaActualizacion,      int idProducto  )
        {
            this. idmovimiento= idmovimiento;
            this.tipoMovimiento=tipoMovimiento;
            this.subTipo=subTipo;
            this.  cantidad=  cantidad;
            this.  nroLote=  nroLote;
            this.fechaCaducidad=fechaCaducidad;
            this. precioUnitario= precioUnitario;
            this. estado= estado;
            this.fechaActualizacion = fechaActualizacion;
            this.idProducto= idProducto;
          

        }
        /// <summary>
        /// constructor para insert
        /// </summary>
        /// <param name="tipoMovimiento"></param>
        /// <param name="subTipo"></param>
        /// <param name="cantidad"></param>
        /// <param name="nroLote"></param>
        /// <param name="fechaCaducidad"></param>
        /// <param name="precioUnitario"></param>
  
        public movimiento( string tipoMovimiento, string subTipo, int cantidad, int nroLote, DateTime fechaCaducidad, decimal precioUnitario)
        {
         
            this.tipoMovimiento = tipoMovimiento;
            this.subTipo = subTipo;
            this.cantidad = cantidad;
            this.nroLote = nroLote;
            this.fechaCaducidad = fechaCaducidad;
            this.precioUnitario = precioUnitario;           
       


        }
        public movimiento()
        {

        }
        #endregion
    }
}
