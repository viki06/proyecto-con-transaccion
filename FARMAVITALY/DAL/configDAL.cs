﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public sealed class configDAL
    {


        public  DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT pathPortadaFotos
                                FROM config";

            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

    }
}
