﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace FARMAVITALY.Proveedor
{
    /// <summary>
    /// Lógica de interacción para vwProveedor.xaml
    /// </summary>
    public partial class vwProveedor : Window
    {
        Producto.vwBuscarProducto vwBuscarProduco;
        byte opcion=0;
        Location puntoUbicacion = null;
        private Common.proveedor proveed;
        private ProveedorBRL brl;
        List<detalleproveedor> detalle;
        producto product;
        DataTable dt;
      
        public proveedor Proveed { get => proveed; set => proveed = value; }

        public vwProveedor(DataTable dt, producto product, proveedor proveed)
        {
            InitializeComponent();
            this.dt = dt;
            this.product = product;
            this.proveed = proveed;

        }
        public vwProveedor(Producto.vwBuscarProducto vwBuscarProduco)
        {
            InitializeComponent();
            this.vwBuscarProduco = vwBuscarProduco;
        }
        public vwProveedor()
        {
            InitializeComponent();
            
        }
        void Habilitar(byte op)
        {
            this.opcion = op;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;
            txtNombre.Focus();
        }

        void DesHabilitar()
        {
            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;
        }
        void FillDataGrid()
        {
            try
            {
                brl = new ProveedorBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR EN FILLDATAGRID"+ ex.Message);
            }
        }
        void BuscarFillDataGrid()
        {
            try
            {
                brl = new ProveedorBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.SelectBuscarProveedor(txtBuscar.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;


            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR al buscar" + ex.Message);
            }
        }
        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {


            Habilitar(1);
            //try
            //{
            //    proveed = new proveedor(txtNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, int.Parse(txtTelefono.Text), puntoUbicacion.Latitude, puntoUbicacion.Longitude, txtNombreCorreo.Text + cbxCorreo.Text);
            //    detalle = new List<detalleproveedor>();
            //    TextBlock idProducto;
            //    for (int i = 0; i < DgvDatos.Items.Count; i++)
            //    {
            //        idProducto = DgvDatos.Columns[0].GetCellContent(DgvDatos.Items[i]) as TextBlock;
            //        detalleproveedor det = new detalleproveedor(product.IdProducto, proveed.IdProveedor);
            //        detalle.Add(det);
            //    }

            //    brl = new ProveedorBRL(proveed, detalle);
            //    brl.Insert();

            //    MessageBox.Show("La Venta Realizado Con Exito ");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);

            //}
            //else
            //{
            //    MessageBox.Show("Debe requerir un Medicamento");
            //}
        }






        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);

        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {

            if (DgvDatos.SelectedItem != null && Proveed != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new ProveedorBRL(Proveed);
                        brl.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
              switch (opcion)
            {
                case 1:

                    try
                    {
                        producto product = new producto();
                        proveedor Proveed = new proveedor();
                        Proveed = new proveedor(txtNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, int.Parse(txtTelefono.Text),puntoUbicacion.Latitude,puntoUbicacion.Longitude, txtNombreCorreo.Text + cbxCorreo.Text);
                        detalle = new List<detalleproveedor>();
                        TextBlock IdProveedor;
                        for (int i = 0; i < DgvDatos.Items.Count; i++)
                        {
                            IdProveedor = DgvDatos.Columns[0].GetCellContent(DgvDatos.Items[i]) as TextBlock;
                            detalleproveedor det = new detalleproveedor(product.IdProducto,Proveed.IdProveedor);
                            detalle.Add(det);
                        }                    
                    brl = new ProveedorBRL(Proveed, detalle);
                    brl.Insert();
                    FillDataGrid();
                    MessageBox.Show("proveedor insertado con exito...");

                    DesHabilitar();

                        }
                    catch (Exception ex)
            {

                MessageBox.Show("ERROR AL INSERTAR" + ex.Message);
            }
        
                    break;

                case 2:
                    try
                    {
                        Proveed.Nombre = txtNombre.Text;
                        Proveed.PrimerApellido = txtPrimerApellido.Text;
                        Proveed.SegundoApellido = txtSegundoApellido.Text;
                        Proveed.Telefono = int.Parse(txtTelefono.Text);
                        Proveed.CorreoElectronico = txtNombreCorreo.Text;
                      
                        brl = new ProveedorBRL(Proveed);
                        brl.Update();
                        FillDataGrid();
                        MessageBox.Show(" categoria Modificado con exito...");
                         DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar " + ex.Message);
                    }
                    break;
            }
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close(); 

        }

        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Proveed = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new ProveedorBRL();
                    Proveed = brl.Get(id);
                    if (Proveed != null)
                    {
                        txtNombre.Text = Proveed.Nombre;
                        txtPrimerApellido.Text = Proveed.PrimerApellido;
                        txtSegundoApellido.Text = Proveed.SegundoApellido;
                        txtTelefono.Text = Proveed.Telefono.ToString();
                        txtNombreCorreo.Text = Proveed.CorreoElectronico;

                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error en el get " + ex.Message);
                }

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
        }

        private void BtnAcercar_Click(object sender, RoutedEventArgs e)
        {
            maps.Focus();
            maps.ZoomLevel++;
        }

        private void BtnAlejar_Click(object sender, RoutedEventArgs e)
        {
            maps.Focus();
            maps.ZoomLevel--;
        }

        private void BtnCalles_Click(object sender, RoutedEventArgs e)
        {
            maps.Focus();
            maps.Mode = new RoadMode();
        }

        private void BtnSatelite_Click(object sender, RoutedEventArgs e)
        {
            maps.Focus();
            maps.Mode = new AerialMode(true);

        }

        private void Maps_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var mousePosicion = e.GetPosition((UIElement)sender);
            puntoUbicacion = maps.ViewportPointToLocation(mousePosicion);

            Pushpin marcador = new Pushpin();
            marcador.Location = puntoUbicacion;

            maps.Children.Clear();
            maps.Children.Add(marcador);
            //  maps.SetView(puntoUbicacion, maps.ZoomLevel);

        }

        private void MIenviar_Click(object sender, RoutedEventArgs e)
        {
            if (Proveed!=null)
            {
                Generica.id = Proveed.IdProveedor;
                Generica.dato1 = Proveed.CorreoElectronico;
                this.Close();
                vwCorreo vw = new vwCorreo();
                vw.ShowDialog();

            }
            else
            {
                MessageBox.Show("DEBE SELECCIONAR UN CORREO");
            }

        }

        private void TxtBuscar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscar.Text=="")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscar.Text.Length>=3)
                {
                    BuscarFillDataGrid();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }
    }
}
