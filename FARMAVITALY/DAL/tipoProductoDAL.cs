﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class tipoProductoDAL : AbstractDAL
    {

        tipoProducto tipoProduct;

        public tipoProducto TipoProduct { get => tipoProduct; set => tipoProduct = value; }


        public tipoProductoDAL()
        {

        }
        public tipoProductoDAL( tipoProducto tipoProduct)
        {
            this.tipoProduct = tipoProduct;
        }
        public override void Delete()
        {
            string query = @"update tipoproducto set estado=0
                             where idTipoProducto=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", tipoProduct.IdTipoProducto);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Insert()
        {
            string query = @"INSERT INTO tipoproducto(nombre)
                            values(@nombre)";

            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", tipoProduct.Nombre);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT  idTipoProducto,nombre AS 'NOMBRE',fechaActualizacion AS 'FECHA DE MODIFICACION' 
                            FROM tipoproducto
                            WHERE estado=1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {
            string query = @"update tipoproducto set   nombre= @nombre,fechaActualizacion=CURRENT_TIMESTAMP
                                where idTipoProducto = @id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", tipoProduct.IdTipoProducto);
                cmd.Parameters.AddWithValue("@nombre", tipoProduct.Nombre);
            
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tipoProducto Get(int idTipoProducto)
        {
            tipoProducto res = null;

            string query = @" SELECT  idTipoProducto,nombre,estado,fechaActualizacion
                                 FROM tipoproducto
                                    WHERE idTipoProducto=@id";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idTipoProducto);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new tipoProducto(int.Parse(dr[0].ToString()), dr[1].ToString(), byte.Parse(dr[2].ToString()), DateTime.Parse(dr[3].ToString()));
                }

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }
            return res;
        }
        public DataTable BuscarTipoProducto(string texto)
        {
            DataTable res = new DataTable();
            string query = @" SELECT idTipoProducto,nombre AS 'NOMBRE DE TIPO PRODUCTO',fechaActualizacion AS 'FECHA DE ACTUALIZACION' FROM tipoproducto
                                WHERE nombre LIKE @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + texto + "%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        public DataTable SelectIDName()
        {

            DataTable res = new DataTable();
            string query = @"SELECT  idTipoProducto,nombre
                            FROM tipoproducto
                            WHERE estado=1
                            ORDER BY 2";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return res;
        }

       
    }
}
