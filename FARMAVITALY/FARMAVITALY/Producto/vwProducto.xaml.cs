﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;
using Microsoft.Win32;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace FARMAVITALY.Producto
{
    /// <summary>
    /// Lógica de interacción para vwProducto.xaml
    /// </summary>
    public partial class vwProducto : System.Windows.Window
    {
        producto producto;
        ProductoBRL brlProducto;
        ProveedorBRL brlProveedor;
        CategoriaBRL brlCategoria;
        tipoProductoBRL brlTipoProducto;
        Validaciones obj = new Validaciones();
        string pathFotografia = string.Empty;

        int posicion = -1  ;
         List<movimiento> fotos = new List<movimiento>();
        //producto brl;
        /// <summary>
        /// 0 insertar , 1 modificar , -1 sin accion
        /// </summary>
        byte opcion = 0;
        public vwProducto()
        {
            InitializeComponent();
        }
        void Habilitar(byte op)
        {
            this.opcion =op;
            grdDatos.IsEnabled = true;
            btnInsertar.IsEnabled = false;
             btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;

            txtNombreProducto.Text = string.Empty;
            txtPrecioCompra.Text = string.Empty;
            txtPrecioVenta.Text = string.Empty;
            txtPresentacion.Text = string.Empty;

            txtNombreProducto.Focus();

        }

        void DesHabilitar()
        {

            txtNombreProducto.Text = string.Empty;
            txtPrecioCompra.Text = string.Empty;
            txtPrecioVenta.Text = string.Empty;
            txtPresentacion.Text = string.Empty; 
            btnInsertar.IsEnabled = true;


            grdDatos.IsEnabled = false;
            btnInsertar.IsEnabled = true;
            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;
        }
        void FillDataGrid()
        {
            try
            {
                brlProducto = new ProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brlProducto.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        void BuscarProducto()
        {
            try
            {

                brlProducto = new ProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brlProducto.BuscarProductoNombre(txtBuscarProducto.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        void FillComboProveedor()
        {
            try
            {
                brlProveedor = new ProveedorBRL();
                cbxProveedor.DisplayMemberPath = "nombre";
                cbxProveedor.SelectedValuePath = "idProveedor";
                cbxProveedor.ItemsSource = brlProveedor.SelectIDNameProveedor().DefaultView;
                cbxProveedor.SelectedIndex = 0;


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "comboBook categoria");
            }
        }
        void FillComboCategoria()
        {
            try
            {
                brlCategoria = new CategoriaBRL();
                cbxCategoria.DisplayMemberPath = "nombre";
                cbxCategoria.SelectedValuePath = "idCategoria";
                cbxCategoria.ItemsSource = brlCategoria.SelectIDName().DefaultView;
                cbxCategoria.SelectedIndex = 0;


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message+"comboBook categoria");
            }
        }


      
        void FillComboTipoProducto()
        {
            try
            {

                brlTipoProducto = new tipoProductoBRL();
            
                cbxTipoProducto.DisplayMemberPath = "nombre";
                cbxTipoProducto.SelectedValuePath = "idTipoProducto ";
                 cbxTipoProducto.ItemsSource = brlTipoProducto.SelectIDName().DefaultView;
                cbxTipoProducto.SelectedIndex = 0;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message+"tipo  XXX producto");
            }
        }
        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);

        }
    
        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
          
        }


        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            switch (opcion)
            {
                case 1:
                    if (true)
                    {
                        try
                        {
                     producto = new producto(txtNombreProducto.Text,Decimal.Parse(txtPrecioCompra.Text), Decimal.Parse(txtPrecioVenta.Text),txtNumLote.Text,DateTime.Parse(dtFechaCaducidad.Text),int.Parse(txtStock.Text),txtPresentacion.Text,int.Parse(cbxCategoria.SelectedValue.ToString()),int.Parse(cbxTipoProducto.SelectedValue.ToString()));
                       brlProducto = new ProductoBRL(producto);
                       brlProducto.Insert();
                        MessageBox.Show("registro insertado con exito");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    
                    break;

                case 2:
                    try
                    {
                        producto.IdCategoria=int.Parse(cbxCategoria.Text);
                        producto.IdTipoProducto = int.Parse(cbxTipoProducto.Text);
                        producto.NombreProducto = txtNombreProducto.Text;
                        producto.PrecioCompra = int.Parse(txtPrecioCompra.Text);
                        producto.PrecioVenta = int.Parse(txtPrecioVenta.Text);
                        producto.Stock = int.Parse(txtStock.Text);
                        producto.Presentacion = txtPresentacion.Text;
                        producto.NroLote = txtNumLote.Text;
                        producto.FechaCaducidad =DateTime.Parse(dtFechaCaducidad.Text);   
               
                        brlProducto = new ProductoBRL(producto);
                        brlProducto.Update();
                        MessageBox.Show(" Producto Modificado con exito...");
                        FillDataGrid();
                        DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                         MessageBox.Show(ex.Message);
                    }
                    break;
            }
        }

        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            producto = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brlProducto = new ProductoBRL();
                    producto = brlProducto.Get(id);
                    if (producto!= null)
                    {
                        txtNombreProducto.Text = producto.NombreProducto;
                        txtPrecioCompra.Text = producto.PrecioCompra.ToString();
                        txtPrecioVenta.Text = producto.PrecioVenta.ToString();
                        txtNumLote.Text = producto.NroLote;
                        dtFechaCaducidad.Text = producto.FechaCaducidad.ToString(); //.ParseExact("04/18/2013","dd/MM/yyyy",
                        txtStock.Text = producto.Stock.ToString();
                        txtPresentacion.Text = producto.Presentacion;
                        
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error en el get  sssss" + ex.Message);
                }

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
            FillComboTipoProducto();
            FillComboCategoria();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //private void BtnFotografia_Click(object sender, RoutedEventArgs e)
        //{
        //    OpenFileDialog ofd = new OpenFileDialog();
        //    ofd.Filter = "Archivos  de Imagenes|*.jpg";
        //    if (ofd.ShowDialog()==true)
        //    {
        //        imgProducto.Source = new BitmapImage(new Uri(ofd.FileName));
        //        pathFotografia = ofd.FileName;
        //    }
        //    //ofd.ShowDialog();
        //}

        private void BtnAtras_Click(object sender, RoutedEventArgs e)
        {
            if (fotos.Count > 0)
            {
                if (posicion > 0)
                {
                    posicion--;
                    //imgProducto.Source = new BitmapImage(new Uri(fotos[posicion].Ubicacion));
                    //lblDescripcionFotografia.Content = fotos[posicion].Descripcion;
                }
            }
        }

        private void BtnAdelante_Click(object sender, RoutedEventArgs e)
        {
            if (fotos.Count>0)
            {
                if (posicion<fotos.Count-1)
                {
                    posicion++;
                    //    imgProducto.Source = new BitmapImage(new Uri(fotos[posicion].Ubicacion));
                    //    lblDescripcionFotografia.Content = fotos[posicion].Descripcion;
                }
            }
        }

        //private void BtnAgregarJPG_Click(object sender, RoutedEventArgs e)
        //{
        //    txtDescripcionJPG.Text = txtDescripcionJPG.Text.Trim();
        //    if (pathFotografia != string.Empty)
        //    {
        //        //fotos.Add(new foto(txtDescripcionJPG.Text, pathFotografia));
        //        txtDescripcionJPG.Text = string.Empty;
        //        pathFotografia = string.Empty;
        //        imgProducto.Source = null;
        //        lblInfo.Content = "jpg añadida a la  lista";
        //    }
        //    else
        //    {
        //        lblInfo.Content = "Debe elegir una foto a ingresar una descripcion";
        //    }
            
        //}

        private void BtnElimiar_Click(object sender, RoutedEventArgs e)
        {
            if (DgvDatos.SelectedItem != null && producto != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brlProducto = new ProductoBRL(producto);
                        brlProducto.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

    
      

        private void TxtBuscarProducto_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscarProducto.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscarProducto.Text.Length >= 3)
                {
                    BuscarProducto();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }

        private void BtnExcel_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application excel = new Excel.Application();
            excel.Visible = true;
            Workbook workbook = excel.Workbooks.Add(System.Reflection.Missing.Value);
            Worksheet sheet1 = (Worksheet)workbook.Sheets[1];
            try
            {
                for (int j = 1; j < DgvDatos.Columns.Count; j++)
                {
                    Range myRange = (Range)sheet1.Cells[j];
                    sheet1.Cells[j].Font.Bold = true;
                    sheet1.Columns[j].ColumnWidth = 15;
                    myRange.Value2 = DgvDatos.Columns[j].Header;
                }
                for (int j = 0; j < DgvDatos.Items.Count; j++)
                {
                    for (int i = 1; i < DgvDatos.Columns.Count; i++)
                    {
                        Range myRange = (Range)sheet1.Cells[j + 2, i];
                        myRange.Value2 = ((DataRowView)DgvDatos.Items[j]).Row[i].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
    }
}
