﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public class UsuarioBRL : AbstractBRL
    {

        private UsuarioDAL dal;
        public Usuario user;


        public Usuario Usuario { get => user; set => user = value; }
        public UsuarioDAL Dal { get => dal; set => dal = value; }

        public UsuarioBRL()
        {
            dal = new UsuarioDAL();
        }
           


        public UsuarioBRL(Usuario user)
        {
            this.user = user;
            dal = new UsuarioDAL(this.user);
        }

        public override void Delete()
        {
            dal.Delete();
        }

        public override void Insert()
        {
            dal.Insert();
        }

        public override DataTable Select()
        {
            return dal.Select();
        }
     
        public DataTable SELECTCI(string ci)
        {
            return dal.SELECTCI(ci);
        }
        public DataTable BuscarUsuarioNombrePrimerAp(string texto)
        {
            return dal.BuscarUsuarioNombrePrimerAp(texto);
        }
        public override void Update()
        {
            dal.Update();
        }
        

        public Usuario Get(int IdUsuario)
        {
            return dal.Get(IdUsuario);
        }
        public DataTable Login(string usuario, string password)
        {
            dal = new UsuarioDAL();
            return dal.Login(usuario, password);
        }
        public void Password(int id, string passw)
        {
            dal.Password(id, passw);
        }

       
    }     
    }


