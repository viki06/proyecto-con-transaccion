﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ReporteDAL : AbstractDAL
    {

        public ReporteDAL()
        {

        }
        public override void Delete()
        {
            throw new NotImplementedException();
        }
        public override void Insert()
        {
            throw new NotImplementedException();
        }
         public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT * FROM bd.reporte";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
