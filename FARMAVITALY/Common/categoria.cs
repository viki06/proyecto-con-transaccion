﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
 public   class categoria
    {
        #region atributos y Methods det y set de la clase categoria
        private int      idCategoria;
        private string   nombre;
        private string   descripcion;
        private byte        estado;
        private DateTime fechaActualizacion;

        public int IdCategoria { get => idCategoria; set => idCategoria = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public byte Estado { get => estado; set => estado = value; }
        public DateTime FechaActualizacion { get => fechaActualizacion; set => fechaActualizacion = value; }

        #endregion

        #region sobre carga de constructor
        /// <summary>
        /// constructor para completo para get
        /// </summary>
        /// <param name="idCategoria"></param>
        /// <param name="nombre"></param>
        /// <param name="descripcion"></param>
        /// <param name="estado"></param>
        /// <param name="fechaActualizacion"></param>
        public categoria(int idCategoria, string nombre, string descripcion, byte estado, DateTime fechaActualizacion)
        {
            this. idCategoria= idCategoria;
            this. nombre= nombre;
            this. descripcion= descripcion;
            this.    estado=    estado;
            this.fechaActualizacion = fechaActualizacion;

        }
        /// <summary>
        /// constructor para insert
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="descripcion"></param>
        /// <param name=""></param>
        public categoria( string nombre, string descripcion )
        {
          
            this.nombre = nombre;
            this.descripcion = descripcion;
          
        }

        public categoria()
        {

        }

        #endregion

    }
}
