﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Common;
using BRL;
using System.Data;

namespace FARMAVITALY.Producto
{
    /// <summary>
    /// Lógica de interacción para vwTipoProducto.xaml
    /// </summary>
    public partial class vwTipoProducto : Window
    {
        public vwTipoProducto()
        {
            InitializeComponent();
        }
        byte opcion = 0;
        Common.tipoProducto tipoProduct;
        tipoProductoBRL brl;

        void Habilitar(byte op)
        {
            this.opcion = op;
            btnInsertar.IsEnabled = false;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;

            btnGuardar.IsEnabled = true;
            btnCancelar.IsEnabled = true;
            DgvDatos.IsEnabled = true;
            txtNombre.Focus();
        }
         void  clearTxt()
        {
            txtNombre.Text = "";
            txtBuscarTipoProducto.Text = "";
        }
        void DesHabilitar()
        {

            btnInsertar.IsEnabled = true;
            btnModificar.IsEnabled = true;
            btnEliminar.IsEnabled = true;

            btnGuardar.IsEnabled = false;
            btnCancelar.IsEnabled = false;
            DgvDatos.IsEnabled = false;
        }
        void FillDataGrid()
        {
            try
            {
                brl = new tipoProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.Select().DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        void FillBuscarTipoProducto()
        {
            try
            {
                brl = new tipoProductoBRL();
                DgvDatos.ItemsSource = null;
                DgvDatos.ItemsSource = brl.BuscarTipoProducto(txtBuscarTipoProducto.Text).DefaultView;
                DgvDatos.Columns[0].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(1);
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            Habilitar(2);
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DesHabilitar();
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (DgvDatos.SelectedItem != null && tipoProduct != null)
            {
                if (MessageBox.Show("esta realmente segur@ de eleminar el registro??", "Eleminar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        brl = new tipoProductoBRL(tipoProduct);
                        brl.Delete();
                        FillDataGrid();
                        MessageBox.Show("Registro Eleminado con exito");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {

            switch (opcion)
            {
                case 1:

                    try
                    {
                        tipoProduct = new Common.tipoProducto(txtNombre.Text);
                        brl = new tipoProductoBRL(tipoProduct);
                        brl.Insert();
                        MessageBox.Show("tipo de Producto insertado con exito...");
                        clearTxt();
                        FillDataGrid();
                        DesHabilitar();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }
                    break;

                case 2:
                    try
                    {
                        tipoProduct.Nombre = txtNombre.Text;
                       

                        brl = new tipoProductoBRL(tipoProduct);
                        brl.Update();
                        MessageBox.Show(" tipo de Producto Modificado con exito...");
                        clearTxt();
                        FillDataGrid();
                        DesHabilitar();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Error al modificar " + ex.Message);
                    }
                    break;
            }
        }

        private void DgvDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            tipoProduct = null;
            if (DgvDatos.SelectedItems != null && DgvDatos.Items.Count > 0)
            {
                try
                {
                    DataRowView dataRow = (DataRowView)DgvDatos.SelectedItem;
                    int id = int.Parse(dataRow.Row.ItemArray[0].ToString());
                    brl = new tipoProductoBRL();
                    tipoProduct = brl.Get(id);
                    if (tipoProduct != null)
                    {
                        txtNombre.Text = tipoProduct.Nombre;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error en el get " + ex.Message);
                }

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
            FillBuscarTipoProducto();
        }

        private void TxtBuscarTipoProducto_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBuscarTipoProducto.Text == "")
            {
                DgvDatos.ItemsSource = null;
            }
            else
            {
                if (txtBuscarTipoProducto.Text.Length >= 3)
                {
                    FillBuscarTipoProducto();
                }
                else
                {
                    DgvDatos.ItemsSource = null;
                }
            }
        }
    }
}
