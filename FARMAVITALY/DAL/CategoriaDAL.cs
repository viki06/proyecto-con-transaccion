﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace DAL
{
    public class CategoriaDAL : AbstractDAL
    {
        public categoria cat;

        public categoria Cat
        {
            get   {  return cat;  }
            set   { cat = value;  }
        }
        public CategoriaDAL(categoria cat)
        {
            this.cat = cat;
        }
        public CategoriaDAL()
        {
        }

        public override void Insert()
        {
            string query = @"insert into categoria(Nombre,Descripcion)
                            values(@Nombre, @Descripcion)";

            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@Nombre", cat.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", cat.Descripcion);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override void Update()
        {
            string query = @"update categoria set   Nombre = @Nombre,Descripcion = @Descripcion ,fechaActualizacion=CURRENT_TIMESTAMP
                                where idCategoria = @id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", cat.IdCategoria);
                cmd.Parameters.AddWithValue("@Nombre", cat.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", cat.Descripcion);
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override void Delete()
        {
            string query = @"update categoria set estado=0
                             where idCategoria=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", cat.IdCategoria);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override DataTable Select()
        {
            DataTable res = new DataTable();
            string query = @"SELECT idCategoria ,nombre AS 'NOMBRE DE CATEGORIA',descripcion AS 'DESCRIPCION DE CATEGORIA',fechaActualizacion AS 'FECHA DE ACTUALIZACION'
                                FROM categoria
                                WHERE estado=1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }




        public categoria Get(int idCategoria)
        {
            categoria res = null;

            string query = @"select idCategoria,nombre,descripcion,estado,fechaActualizacion
                                   from	 categoria 
                                    where idCategoria=@id";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idCategoria);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new categoria(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), byte.Parse(dr[3].ToString()), DateTime.Parse(dr[4].ToString()));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }
        public DataTable SelectIDName()
        {

            DataTable res = new DataTable();
            string query = @"SELECT  idCategoria,nombre,descripcion
                            from categoria
                            where estado = 1
                            order by 2";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return res;
        }

        public  DataTable BuscarCategoria(string texto)
        {
            DataTable res = new DataTable();
            string query = @" SELECT idCategoria,nombre,descripcion,fechaActualizacion FROM  categoria
                                WHERE nombre LIKE @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto", "%" + texto + "%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
    }
}


