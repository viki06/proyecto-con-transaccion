﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class ProductoDAL : AbstractDAL
    {
        producto product;
        public producto Product { get => product; set => product = value; }

        public ProductoDAL()
        {

        }
     
        public ProductoDAL(producto product)
        {
            this.Product = product;
         
        }
        public int GetAutoincrementID()
        {
            return Methods.GetAutoIncrementTable("producto");
        }
        public  DataTable BuscarProductoNombre(string texto)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idProducto,nombreProducto,precioCompra,precioVenta,nroLote,fechaCaducidad,stock,presentacion
                                FROM producto
                                WHERE nombreProducto  LIKE @texto OR presentacion LIKE  @texto";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@texto","%"+texto+"%");
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        



        }
        public override void Delete()
        {
            string query = @"UPDATE producto SET estado=0
                             WHERE idProducto=@id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", Product.IdProducto);

                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void Insert()
        {
        string queryProducto = @"insert into producto(nombreProducto, precioCompra, precioVenta,nroLote ,fechaCaducidad stock, presentacion,   idTipoProducto, idCategoria)
       values(@nombreProducto, @precioCompra, @precioVenta,@nroLote,@fechaCaducidad, @stock, @presentacion,  @idTipoProducto, @idCategoria)";
                MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(queryProducto);
                cmd.CommandText = queryProducto;
                cmd.Parameters.AddWithValue("@nombreProducto", Product.NombreProducto);
                cmd.Parameters.AddWithValue("@precioCompra", Product.PrecioCompra);
                cmd.Parameters.AddWithValue("@precioVenta", Product.PrecioVenta);
                cmd.Parameters.AddWithValue("@nroLote", Product.NroLote);
                cmd.Parameters.AddWithValue("@fechaCaducidad", Product.FechaCaducidad);
                cmd.Parameters.AddWithValue("@stock", Product.Stock);
                cmd.Parameters.AddWithValue("@presentacion", Product.Presentacion);
                cmd.Parameters.AddWithValue("@idTipoProducto", Product.IdTipoProducto);
                cmd.Parameters.AddWithValue("@idCategoria", Product.IdCategoria);

               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public override DataTable Select()
        {
            // FALTA HACER HINNER JOIN DE TRES TABLAS
            DataTable res = new DataTable();
            string query = @"SELECT idProducto,nombreProducto AS 'NOMBRE DEL PRODUCTO',precioCompra AS 'PRECIO DE COMPRA',precioVenta AS 'PRECIO DE VENTA',nroLote AS 'Nro LOTE',fechaCaducidad AS 'FECHA DE CADUCIDAD',stock AS 'STOCK',presentacion AS 'PRESENTACION',fechaActualizacion AS 'FECHA DE ACTUALIZACION',idTipoProducto AS 'TIPO DE PRODUCTO',idCategoria AS 'CATEGORIA'
                            FROM producto
                            WHERE estado=1";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;
        }

        public  DataTable SelectStockProducto(int id)
        {
            // FALTA HACER HINNER JOIN DE TRES TABLAS
            DataTable res = new DataTable();
            string query = @"SELECT  stock FROM producto
                            WHERE idProducto=@idProducto";

            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idProducto",id);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        /// <summary>
        /// lista de productos
        /// </summary>
        /// <returns></returns>
        public  DataTable ListaProducto()
        {
            DataTable res = new DataTable();
            string query = @"SELECT idProducto , nombreProducto AS 'NOMBRE DEL PRODUCTO',presentacion AS 'PRESENTACION',precioVenta AS 'PRECIO VENTA',nroLote AS 'Nro LOTE',fechaCaducidad AS 'FECHA CADUCIDAD',stock AS 'STOCK',fechaActualizacion AS 'FECHA DE ACTUALIZACION'
                                FROM producto
                                WHERE estado=1 ";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
       
        public DataTable SeleccionarProducto(int id)
        {
            DataTable res = new DataTable();
            string query = @"SELECT idProducto , nombreProducto AS 'NOMBRE DEL PRODUCTO',precioVenta AS 'PRECIO DEL MEDICAMENTO' ,IF(stock>=1,'DISPONIBLE','AGOTADO' )  AS 'STOCK',fechaActualizacion AS 'FECHA DE ACTUALIZACION'
                                FROM producto
                                WHERE estado=1 ";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                   cmd.Parameters.AddWithValue("@idProducto", id);
                res = Methods.ExecuteDataTableCommand(cmd);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;
        }
        public override void Update()
        {
            string query = @"update producto set  nombreProducto=@nombreProducto, precioCompra=@precioCompra, precioVenta=@precioVenta,nroLote=@nroLote,fechaCaducidad=@fechaCaducidad, stock=@stock, presentacion=@presentacion,fechaActualizacion=CURRENT_TIMESTAMP
                                where idProducto = @id";
            MySqlCommand cmd;
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreProducto", Product.NombreProducto);
                cmd.Parameters.AddWithValue("@precioCompra", Product.PrecioCompra);
                cmd.Parameters.AddWithValue("@precioVenta", Product.PrecioVenta);
                cmd.Parameters.AddWithValue("@nroLote", Product.NroLote);
                cmd.Parameters.AddWithValue("@fechaCaducidad", Product.FechaCaducidad);
                cmd.Parameters.AddWithValue("@stock", Product.Stock);
                cmd.Parameters.AddWithValue("@presentacion", Product.Presentacion);
                cmd.Parameters.AddWithValue("@idTipoProducto", Product.IdTipoProducto);
                cmd.Parameters.AddWithValue("@idCategoria", Product.IdCategoria);
                cmd.Parameters.AddWithValue("@id", Product.IdProducto);
               
                Methods.ExecuteBasicComand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public producto Get(int idProducto)
        {
            producto res = null;

  string query = @"select idProducto,nombreProducto,precioCompra,precioVenta,nroLote,fechaCaducidad,stock,presentacion,estado,fechaActualizacion,idTipoProducto,idCategoria
                                   from	 producto 
                                    where idProducto=@id";

            MySqlCommand cmd = null;
            MySqlDataReader dr = null;

            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idProducto);
                dr = Methods.ExecuteDataReaderCommand(cmd);

                while (dr.Read())
                {
                    res = new producto(int.Parse(dr[0].ToString()),dr[1].ToString(), decimal.Parse(dr[2].ToString()), decimal.Parse(dr[3].ToString()), dr[4].ToString(),DateTime.Parse(dr[5].ToString()),int.Parse(dr[6].ToString()), dr[7].ToString(),byte.Parse(dr[8].ToString()), DateTime.Parse(dr[9].ToString()),  int.Parse(dr[10].ToString()), int.Parse(dr[11].ToString()));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dr.Close();
                cmd.Connection.Close();
            }

            return res;

        }


    }
}
